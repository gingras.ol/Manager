from manager.PostProd import KptMesh, Hk
import numpy as np
from scipy.interpolate import RegularGridInterpolator as GridInt
from itertools import product


ndim = 6

mesh = KptMesh.read("system.klist", [10, 10, 10], 139)
eig = Hk(mesh.nkpt, ndim, mesh)
eig.read("system.Hk")

mesh2 = KptMesh([20, 20, 1], 139)

m = mesh
meig = np.zeros((2*m.n[0]+1, 2*m.n[1]+1, 2*m.n[2]+1,
                 ndim, ndim), dtype=complex)

q_set = product(range(2*m.n[0]+1), range(2*m.n[1]+1),
                range(2*m.n[2]+1))
for qx, qy, qz in q_set:
    q = m.translate_in_FBZ([qx-1, qy-1, qz-1])
    meig[qx, qy, qz] = eig[m[q[0], q[1], q[2]]]

interpol = np.zeros((ndim, ndim),
                    dtype=GridInt)
for l1, l2 in product(range(ndim), range(ndim)):
    linx = np.linspace(-1, 2*m.n[0]-1, 2*m.n[0]+1)
    liny = np.linspace(-1, 2*m.n[1]-1, 2*m.n[1]+1)
    linz = np.linspace(-1, 2*m.n[2]-1, 2*m.n[2]+1)
    interpol[l1, l2] = GridInt((linx, liny, linz),
                               meig[:, :, :, l1, l2])

int_meig = np.zeros((2*mesh2.n[0], 2*mesh2.n[1], 2*mesh2.n[2],
                     ndim, ndim), dtype=complex)

q_set2 = product(range(2*mesh2.n[0]), range(2*mesh2.n[1]),
                 range(2*mesh2.n[2]))
eig2 = np.zeros((mesh2.n[0]*mesh2.n[1]*mesh2.n[2]*2,
                 ndim, ndim), dtype=complex)
i = 0
for qx, qy, qz in q_set2:
    q2x = 1.*(qx-mesh2.N0.item(0))*m.n[0]/mesh2.n[0]+m.N0.item(0)
    q2y = 1.*(qy-mesh2.N0.item(1))*m.n[1]/mesh2.n[1]+m.N0.item(1)
    q2z = 1.*(qz-mesh2.N0.item(2))*m.n[2]/mesh2.n[2]+m.N0.item(2)
    if mesh2.in_FBZ(mesh2.fromNgetM([qx, qy, qz])):
        mesh2[qx, qy, qz] = i
        mesh2.npts.append([qx, qy, qz])
        for l1, l2 in product(range(ndim), range(ndim)):
            v = interpol[l1, l2](np.array([q2x, q2y, q2z]))
            eig2[i, l1, l2] = v
            int_meig[qx, qy, qz, l1, l2] = v
        i += 1
mesh2.nkpt = len(mesh2.npts)

Eig2 = Hk(mesh2.nkpt, ndim, mesh2)
Eig2.hamiltonian = eig2

Eig2.write("system2.Hk", "system2.klist")
