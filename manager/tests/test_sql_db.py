from manager.sql_db import Connector
from manager.Calculations import Barechi2, Dressed, Eliash
from manager.PostProd import KptMesh, PlotParams, TwoPropagator
from itertools import product as itp
import numpy as np
import sqlite3
import os
import shutil

Ha_to_eV = 27.2113845
dirpath = os.path.dirname(os.path.abspath(__file__)) + "/dir_sql_db"


def test_db_01():
    """Create the test_01 database. Do a bunch of operations,
    then remove it."""
    conn = Connector()  # Connector of database

    # Try to load it to see if it exists, as it shouldn't
    try:
        conn.load(database="test_01")
    except ValueError:
        print("Database do not exists and should not.")

    if conn.conn is not None:
        raise Exception("test_01.db already exists. "
                        "Should not for this test.")

    # Infos of the future test database and creation of it
    infos = {"name": "test_01", "ground_dir": dirpath,
             "db_path": dirpath}
    conn.mk_db_infos(infos)
    conn.load(database="test_01")

    objects = [["barechi2", Barechi2], ["dressed", Dressed],
               ["eliash", Eliash]]

    for obj in objects:
        o = obj[1]("system", conn)
    try:
        o.CreateTable()
    except sqlite3.OperationalError:
        query = "drop table %s" % obj[0]
        conn.Query("execute", query)
        o.CreateTable()
    try:
        o.CreateTable()
        raise Exception("Should not be able to create.")
    except sqlite3.OperationalError:
        print("Should get here.")
    query = "drop table %s" % obj[0]
    conn.Query("execute", query)

    # Remove database
    conn.rm_db_infos("test_01")


def test_pipeline_01():
    """Working in the 'tests' database.
       1. Insert a new Barechi2 calculation called b.
       2. From b, set a new Dressed calculation called d.
       3. From given mesh, and vertex, write d from dressing b.
       4. From calculation d, set a new Eliash calculation called e.
       5. Prepare sbatch file for e.
    """
    treeDir = "test_pipeline_01"
    try:
        b = Barechi2()
    except TypeError:
        pass

    conn = Connector()
    conn.load(database="tests")

    query = ("select * from sqlite_master where type='table' and "
             "name='barechi2'")
    print(conn.Query("select", query))

    ###################
    # BARECHI2 OBJECT #
    ###################
    # 1. Insert a new Barechi2 calculation called b
    b = Barechi2("system", conn)
    b_values = {"barechi2dir": "'/%s/Barechi2/'" % treeDir,
                "qpts": "'5x5x2'", "chempot": 6.9432565,
                "niwn": 1024, "nivn": 2, "temp": 116,
                "norb": 3, "nspin": 2, "status": "'done'"}
    b.InsertNew(b_values)

    ##################
    # DRESSED OBJECT #
    ##################
    # The launch function is an example of how to perform these calculations
    # in a python therminal.
    def launch(U, J):
        # 2. From b, set a new Dressed calculation called d
        d = b.SetDressed()
        d.SetBaseDir("/%s/Dressed/" % treeDir)
        d_params = {"dressedU": U, "dressedJ": J,
                    "dressedUp": U-2*J, "dressedUpp": U-3*J, "dressedJp": J,
                    "miwn": 64, "mivn": 2}
        d.SetParams(params=d_params, verify=False)
        # Make the folder tree with need links
        d.MakeTree()
        # Load the full history of dressed: join all preceeding tables
        d.Load(True)

        # 3. Write d from dressing b
        chi0 = b.ReturnBareSusph(nkpt=100)
        d.Dressing(chi0=chi0)
        return d
    d = launch(U=0.2, J=0.04)

    #################
    # ELIASH OBJECT #
    #################
    # 4. From calculation d, set a new Eliash calculation called e
    e = d.SetEliash(nspin=2, ndim=6)
    e.SetBaseDir("/%s/Eliash/" % treeDir)
    e_params = {"parity": 0,
                "liwn": 0,
                "livn": 1,
                "liwn_max": 0,
                "nvec": 3}
    e.SetParams(e_params)
    e.MakeTree()
    e.Load(True)

    # 5. Prepare sbatch file for e
    path_eliash_soc = "/path_to_eliash_program/"
    action = ("module load intel/2018.3\n"
              "module load openmpi\n"
              "srun %s > output.txt" % path_eliash_soc)

    filePath = "%s%ssubmit.sbatch" % (conn.groundDir,
                                      e.cells["eliash.eliashdir"])
    print(filePath)
    params = {"filePath": "%s" % filePath,
              "job-name": "Eli#%d" % e.cells["eliash.eliashid"],
              "account": "rrg-prof",
              "ntasks": "1",
              "mem-per-cpu": "1G",
              "time": "1:00:00",
              "do": action}
    e.SetSbatch(params, launch=False, force=True)

    ###################
    # CLEANING FOLDER #
    ###################
    shutil.rmtree(conn.groundDir + "/%s/Eliash/" % treeDir)
    shutil.rmtree(conn.groundDir + "/%s/Dressed/" % treeDir)

    query = "DELETE FROM eliash WHERE eliashid = %d" % e.id
    conn.Query("execute", query)

    query = "DELETE FROM dressed WHERE dressedid = %d" % d.id
    conn.Query("execute", query)

    query = "DELETE FROM barechi2 WHERE barechi2id = %d" % b.id
    conn.Query("execute", query)


def test_barechi2_01():
    conn = Connector("tests")

    b_dir = "/test_barechi2/Q_5x5x2/"
    b_id = conn.select("SELECT barechi2id FROM barechi2 WHERE barechi2dir='%s'"
                       % b_dir)
    b_id = int(b_id[0][0])
    b = Barechi2("system", conn, id=b_id)

    pre_path = "%s%s" % (conn.groundDir, b_dir)
    m_path = "%sInput/system.qlist" % pre_path
    m = KptMesh.read(m_path, [5, 5, 2], 139, _type="klist")

    params = PlotParams()
    params["subplots"] = [2, 3]
    params["indices"] = [[[0, 0], [7, 7], [14, 14]],
                         [[0, 0], [7, 7], [14, 14]]]
    params.default = False

    b.PlotBareSusGrid(m, params)
    os.remove("%sBareSus_v00.png" % pre_path)


def test_barechi2_02():
    conn = Connector("tests")

    b_dir = "/test_barechi2/Qpath257/"
    b_id = conn.select("SELECT barechi2id FROM barechi2 WHERE barechi2dir='%s'"
                       % b_dir)
    b_id = int(b_id[0][0])
    b = Barechi2("system", conn, id=b_id)

    pre_path = "%s%s" % (conn.groundDir, b_dir)

    params = PlotParams()
    params["subplots"] = [2, 2]
    params["indices"] = [[[[0, 0], [7, 7], [14, 14]],
                          [[1, 1], [2, 2], [8, 8]]],
                         [[[0, 0], [7, 7], [14, 14]],
                          [[1, 1], [2, 2], [8, 8]]]]
    params["freqs"] = [0]
    params.default = False

    b.PlotBareSusPath(params)
    os.remove("%sBareSus_v00.png" % pre_path)


def test_barechi_syms():
    # Load barechi calculation
    conn = Connector("tests")
    b_dir = "/test_barechi2/Q_5x5x2/"
    b_id = conn.select("SELECT barechi2id FROM barechi2 WHERE barechi2dir='%s'"
                       % b_dir)
    b_id = int(b_id[0][0])
    b = Barechi2("system", conn, id=b_id)

    # Set up stuff
    nkpt = 100
    nfreq = b.cells["barechi2.nivn"] + 1
    norb = b.cells["barechi2.norb"]
    nnorb = norb**2
    nspin = b.cells["barechi2.nspin"]
    ndim = nspin*norb

    # ### BARE PP SUSCEPTIBILITY ###
    suspp = b.ReturnBareSuspp(nkpt)
    suspp1 = suspp.Re.Obj + 1j*suspp.Im.Obj

    # Check at the pseudospin basis
    A, B = suspp.changeBasis(suspp1, basis1="OSOS", basis2="OOPP")
    suspp1[:, :, A, :] = suspp1[:, :, B, :]
    suspp1[:, :, :, A] = suspp1[:, :, :, B]
    max_suspp = np.zeros((4, 4))
    for i, j in itp(range(4), repeat=2):
        i1, i2 = nnorb*i, nnorb*(i+1)
        j1, j2 = nnorb*j, nnorb*(j+1)
        max_suspp[i, j] = np.amax(np.absolute(suspp1[:, :, i1:i2, j1:j2]))
    print("Max of the Bare Susceptibility in pseudospin basis:")
    print(max_suspp)

    # Check spin space symmetries
    suspp2 = suspp.Re.Obj + 1j*suspp.Im.Obj
    A, B = suspp.changeBasis(suspp2, basis1="OSOS", basis2="OOSS")
    suspp2[:, :, A, :] = suspp2[:, :, B, :]
    suspp2[:, :, :, A] = suspp2[:, :, :, B]

    suspp_spin = np.zeros((4, 4, nkpt, 1, nnorb, nnorb), dtype=complex)
    for s1, s2, s3, s4 in itp(range(nspin), repeat=4):
        tmp_sus = suspp[:, :, 2*nnorb*s1+nnorb*s2:2*nnorb*s1+nnorb*(s2+1),
                        2*nnorb*s3+nnorb*s4:2*nnorb*s3+nnorb*(s4+1)]
        suspp_spin[s1*nspin+s2, s3*nspin+s4] = tmp_sus
    # TEST SYMS HERE

    # ### BARE PH SUSCEPTIBILITY ###
    susph = b.ReturnBareSusph(nkpt)
    susph1 = susph.Re.Obj + 1j*susph.Im.Obj
    A, B = susph.changeBasis(susph1, basis1="OSOS", basis2="OOSS")
    susph1[:, :, A, :] = susph1[:, :, B, :]
    susph1[:, :, :, A] = susph1[:, :, :, B]

    susph_spin = np.zeros((4, 4, nkpt, nfreq, nnorb, nnorb), dtype=complex)
    for s1, s2, s3, s4 in itp(range(nspin), repeat=4):
        tmp_sus = susph[:, :, 2*nnorb*s1+nnorb*s2:2*nnorb*s1+nnorb*(s2+1),
                        2*nnorb*s3+nnorb*s4:2*nnorb*s3+nnorb*(s4+1)]
        susph_spin[s1*nspin+s2, s3*nspin+s4] = tmp_sus

    tp_ph = TwoPropagator(ndim, nkpt, nfreq)

    diff = susph_spin[0, 0] - np.transpose(susph_spin[3, 3], axes=[0, 1, 3, 2])
    print(np.amax(np.amax(np.absolute(diff), axis=0), axis=0))
    assert((np.round(diff, 6) == 0).all)

    diff = susph_spin[0, 0] - tp_ph.partialTranspose(susph_spin[1, 1], "24")
    print(np.amax(np.amax(np.absolute(diff), axis=0), axis=0))
    assert((np.round(diff, 6) == 0).all)

    diff = susph_spin[0, 0] - tp_ph.partialTranspose(susph_spin[2, 2], "13")
    print(np.amax(np.amax(np.absolute(diff), axis=0), axis=0))
    assert((np.round(diff, 6) == 0).all)

    # TEST BARE CHI PP ALSO


def test_dressed_01():
    conn = Connector("tests")

    d_dir = "/test_dressed/Qpath257/"
    d_id = conn.select("SELECT dressedid FROM dressed WHERE dresseddir LIKE "
                       "'" + d_dir + "%" + "'")[0][0]
    d_id = int(d_id)
    d = Dressed("system", conn, id=d_id)

    pre_path = "%s%s" % (conn.groundDir, d.cells["dressed.dresseddir"])

    params = PlotParams()
    params["subplots"] = [1, 2]
    params["indices"] = [[[[0, 0], [7, 7], [14, 14]],
                          [[1, 1], [2, 2], [8, 8]]]]
    params["freqs"] = [0]
    params.default = False

    d.PlotDressedSusPath(params)
    os.remove("%sDressedSus_v0.png" % pre_path)


def test_dressed_pseudospin_ladder():
    conn = Connector("tests")

    nkpt = 100
    freqs = {"nw": 1, "nv": 3, "nw_chi": 2}

    d_dir = "/test_dressed/Q_5x5x2/"
    d_id = conn.select("SELECT dressedid FROM dressed WHERE dresseddir LIKE "
                       "'" + d_dir + "%" + "'")[0][0]
    d_id = int(d_id)
    d = Dressed("system", conn, id=d_id)
    b = Barechi2("system", conn, id=d.cells["barechi2.barechi2id"])

    pre_path = "%s%s" % (conn.groundDir, d.cells["dressed.dresseddir"])
    post_path = "%sPrepPseudo/" % pre_path
    print(post_path)
    if os.path.exists(post_path):
        shutil.rmtree(post_path)
    os.makedirs(post_path)

    bchipp = b.ReturnBareChipp(nkpt)

    d.PreparePseudospinEffectivePairing(nkpt, freqs, write_path=post_path,
                                        verbose=True)
    d.WriteChipp_pm(nkpt, freqs, post_path, bchipp=bchipp, verbose=True)

    treeDir = "%s" % d.cells["dressed.dresseddir"]
    #################
    # ELIASH OBJECT #
    #################
    # 4. From calculation d, set a new Eliash calculation called e
    e = d.SetEliash(nspin=2, ndim=3, pseudo=2)
    e.SetBaseDir("%s/Eliash/" % treeDir)
    e_params = {"parity": 0,
                "liwn": 0,
                "livn": 1,
                "liwn_max": 0,
                "nvec": 3}
    e.SetParams(e_params)
    e.MakeTree()
    e.Load(True)

    # 5. Prepare sbatch file for e
    path_eliash_soc = "/path_to_eliash_program/"
    action = ("module load intel/2018.3\n"
              "module load openmpi\n"
              "srun %s > output.txt" % path_eliash_soc)

    filePath = "%s%ssubmit.sbatch" % (conn.groundDir,
                                      e.cells["eliash.eliashdir"])
    print(filePath)
    params = {"filePath": "%s" % filePath,
              "job-name": "Eli#%d" % e.cells["eliash.eliashid"],
              "account": "rrg-prof",
              "ntasks": "1",
              "mem-per-cpu": "1G",
              "time": "1:00:00",
              "do": action}
    e.SetSbatch(params, launch=False, force=True)

    ###################
    # CLEANING FOLDER #
    ###################
    shutil.rmtree(conn.groundDir + "%s/Eliash/" % treeDir)
    shutil.rmtree(conn.groundDir + "%s/PrepPseudo/" % treeDir)

    query = "DELETE FROM eliash WHERE eliashid = %d" % e.id
    conn.Query("execute", query)


def test_eliash_01():
    conn = Connector("tests")

    e_dir = "/test_eliash/"
    e_id = conn.select("SELECT eliashid FROM eliash WHERE eliashdir LIKE "
                       "'" + e_dir + "%" + "'")[0][0]
    mgx, mgy, mgz = 5, 5, 2
    sgroup = 139
    orbs = ["xy", "yz", "zx"]

    e_id = int(e_id)
    e = Eliash("Sr2RuO4", conn, id=e_id)

    norb, nspin = e.cells["barechi2.norb"], e.cells["barechi2.nspin"]
    ndim = norb*nspin

    pre_path = "%s%s" % (conn.groundDir, e.cells["eliash.eliashdir"])

    post_path = "%s/PostProd" % pre_path
    if os.path.exists(post_path):
        shutil.rmtree(post_path)
    os.makedirs(post_path)

    # Set eigs in the database
    e.SetEigs(outputFile="output.dat")

    # Load gap mesh on the 5x5x2 grid
    mg = KptMesh.read("%sInput/%s.klist" % (pre_path, e.system),
                      [mgx, mgy, mgz], sgroup)
    mg.sgroup.SetOrbs(orbs)

    # Set the irreps of each gap of the calculation in the database
    e.SetIrreps(mg)

    # Now working only with the first eigenvectors
    gap = e.LoadGap(mg, "01")
    irrep = gap.returnIrrep(basis="SpinOrb", precision=5e-5)[0]
    print(irrep)
    gap = gap.IrrepProjection(2)
    # Calculate the SPOT contributions
    # TODO: This part should be revised. It could go in the database, but
    # also dealing with eo_SPOT and eo_SPOT2 is not efficient.
    eo_SPOT, eo_SPOT2 = gap.SPOT_contributions(analysis=True)

    # Plot the gap functions in the gap basis
    # TODO: This is also bad. Giving the eo_SPOT object is weird.
    params = PlotParams()
    params["show"] = True
    params.default = False
    orbs_lab = ["xy", "yz", "zx"]
    spin_lab = [r"\uparrow", r"\downarrow"]
    basis = ["%s%s" % (orbs_lab[int(i % norb)], spin_lab[int(i/norb)])
             for i in range(ndim)]

    gap_i = np.sum(np.sum(np.sum(np.sum(eo_SPOT, axis=0), axis=0), axis=0),
                   axis=0)[0]
    saveplace = "%s/GapFn01.png" % post_path
    gap.plotGap2(gap_i, basis, params, saveplace)

    gap_i = np.sum(np.sum(np.sum(eo_SPOT[:, :, :, 0], axis=0), axis=0),
                   axis=0)[0]
    saveplace = "%s/GapFn01_+T.png" % post_path
    gap.plotGap2(gap_i, basis, params, saveplace)

    shutil.rmtree("%s/PostProd" % pre_path)
