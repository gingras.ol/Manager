from manager.PostProd import KptMesh, Projectors, Gap
# from manager.PostProd import Projectors
from manager.PostProd import Hk, Bogoliubov
# from manager.PostProd import Gap
# from manager.PostProd import TwoPropagator
# from manager.PostProd import Gamma
# import unittest
import os
import numpy as np
from scipy import linalg as ln
import math
from matplotlib import pyplot as plt
import shutil
from itertools import product as itp
# from manager.PostProd import SelfE
# from manager.PostProd import SpectralF


Ha_to_eV = 27.2113845
dirpath = os.path.dirname(__file__) + "/dir_bogo"


def test_bogo_01():
    """From an abinit output with all k points (kptopt = 1) in a
       conventional 10x10x20 BZ (kptrlatt = [[10, 10, 0], [10, 0, 10],
       [0, 10, 10]]), maps all the points in the first BZ.
    """
    filepath = "%s/test_bogo_01" % dirpath

    mhx, mhy, mhz = 15, 15, 2
    mgx, mgy, mgz = 5, 5, 2
    mix, miy, miz = 30, 30, 1
    sgroup = 139
    orbs = ["xy", "yz", "zx"]
    nspin, norb = 2, 3
    nband, nfreq = 12, 2

    post_path = "%s/Output" % dirpath
    if os.path.exists(post_path):
        shutil.rmtree(post_path)
    os.makedirs(post_path)
    ####################################
    # 1. Make the band basis Hamiltonian
    ####################################
    ndim = nspin*norb
    mh = KptMesh.read("%s/Input/calc.out" % filepath, [mhx, mhy, mhz], sgroup)
    mh.sgroup.SetOrbs(["xy", "yz", "zx"])

    h0 = Hk(mh.nkpt, nband, mh)
    h0.read("%s/Input/forlb.eig" % filepath)
    prepath = "%s/1-h0_band" % post_path
    h0.full_mesh_hamilt(write=["%s.Hk" % prepath, "%s.klist" % prepath])

    ###################
    # 2. Interpolate h0
    ###################
    mi = KptMesh([mix, miy, miz], 139)
    h0i = h0.interpolate(mi)
    prepath = "%s/2-inter" % post_path
    h0i.write(hk_out="%s.Hk" % prepath, klist_out="%s.klist" % prepath)
    mi = KptMesh.read("%s.klist" % prepath, [mix, miy, miz], sgroup)

    # Plot
    fermi = 6.9432565
    broad = 0.05
    mu = (fermi + 1j*broad)*np.identity(nband)

    G = np.zeros((mi.nkpt, nband, nband), dtype=complex)
    A = np.zeros((nband, 2*mix, 2*miy))
    for k, kpt in enumerate(mi.npts):
        G[k] = ln.inv(mu - h0i[k])
        for d in range(nband):
            if kpt[2] == 0:
                A[d][kpt[0]][kpt[1]] = - 1./math.pi*np.imag(G[k][d][d])

    fig, ax = plt.subplots(1, nband, figsize=(16, 4))
    cutoff = [0 for i in range(nband)]
    for d in range(nband):
        mask = np.ma.masked_where(A[d] < cutoff[d], A[d])
        ax[d].imshow(mask, origin="lower")
    fig.savefig("%s.png" % prepath)
    # fig.draw()
    # fig.show()

    ##########################################
    # 3. Change gap from orbital to band basis
    ##########################################
    mh = KptMesh.read("%s/Input/calc.out" % filepath,
                      [mhx, mhy, mhz], sgroup)
    mh.sgroup.SetOrbs(["xy", "yz", "zx"])
    proj = Projectors(norb, nband, mh.nkpt, nspin=nspin)
    proj.read("%s/Input/forlb.ovlp" % filepath)
    proj.orthonormalize()

    mg = KptMesh.read("%s/Input/Sr2RuO4.klist" % filepath, [mgx, mgy, mgz],
                      sgroup)
    mg.sgroup.SetOrbs(orbs)

    gap = Gap(ndim, mg, nfreq, nspin=nspin)
    gap.Re.Read("%s/Input/Sr2RuO4.ReGapFn01" % filepath, _skip=1, _skipc=1)
    gap.Im.Read("%s/Input/Sr2RuO4.ImGapFn01" % filepath, _skip=1, _skipc=1)

    gap_b = gap.changeBasis(proj, mh)
    gap_b.write("Sr2RuO4", prepath="%s/3-" % post_path)

    ####################################
    # 4. Interpolate gap, print and plot
    ####################################
    mi2 = KptMesh([mix, miy, miz], 139)
    gapi = gap_b.interpolate(mi2)
    gapi.write("Sr2RuO4", prepath="%s/4-" % post_path)

    with open("%s/4-gap.gap" % post_path, "w") as f:
        with open("%s/4-gap.klist" % post_path, "w") as fk:
            fk.write("%s\n" % mi2.nkpt)
            for k, kpt in enumerate(mi2.npts):
                npt = mi2.fromNgetM(kpt)
                fk.write("%.12f    %.12f    %.12f\n" %
                         (npt[0], npt[1], npt[2]))
                line = ""
                for d, d2 in itp(range(nband), repeat=2):
                    line += ("%.12f   %.12f   " %
                             (gapi.Re[k][0][d][d2], gapi.Im[k][0][d][d2]))
                f.write(line + "\n")

    # Remove Output directory
    shutil.rmtree(post_path)


# def test_bogo_02():
#     filepath = "%s/test_bogo_02" % dirpath
#
#     mhx, mhy, mhz = 48, 48, 48
#     mgx, mgy, mgz = 24, 24, 1
#     # mix, miy, miz = 30, 30, 1
#     # mix, miy, miz = 5, 5, 1
#     sgroup = 139
#     orbs = ["xy", "yz", "zx"]
#     nspin, norb = 1, 3
#     nband, nfreq = 6, 2
#
#     ndim = nspin*norb
#
#     os.makedirs("%s/Output" % filepath)
#     ####################################
#     # 1. Make the band basis Hamiltonian
#     ####################################
#     # mh = KptMesh.read("%s/Input/calc.out" % filepath,
#     #                   [mhx, mhy, mhz], sgroup)
#     # mh.sgroup.SetOrbs(["xy", "yz", "zx"])
#
#     # h0 = Hk(mh.nkpt, nband, mh)
#     # h0.read("%s/Input/forlb.eig" % filepath)
#     # prepath = "%s/Output/1-h0_band" % filepath
#     # h0.full_mesh_hamilt(write=["%s.Hk" % prepath, "%s.klist" % prepath])
#
#     # ###################
#     # # 2. Interpolate h0
#     # ###################
#     # mi = KptMesh([mix, miy, miz], 139)
#     # h0i = h0.interpolate(mi)
#     # prepath = "%s/Output/2-inter" % filepath
#     # h0i.write(hk_out="%s.Hk" % prepath, klist_out="%s.klist" % prepath)
#     # mi = KptMesh.read("%s.klist" % prepath, [mix, miy, miz], sgroup)
#
#     # # Plot
#     # fermi = 6.9432565
#     # broad = 0.05
#     # mu = (fermi + 1j*broad)*np.identity(nband)
#
#     # G = np.zeros((mi.nkpt, nband, nband), dtype=complex)
#     # A = np.zeros((nband, 2*mix, 2*miy))
#     # for k, kpt in enumerate(mi.npts):
#     #     G[k] = ln.inv(mu - h0i[k])
#     #     for d in range(nband):
#     #         if kpt[2] == 0:
#     #             A[d][kpt[0]][kpt[1]] = - 1./math.pi*np.imag(G[k][d][d])
#
#     # fig, ax = plt.subplots(1, nband, figsize=(16, 4))
#     # cutoff = [0 for i in range(nband)]
#     # for d in range(nband):
#     #     mask = np.ma.masked_where(A[d] < cutoff[d], A[d])
#     #     ax[d].imshow(mask, origin="lower")
#     # fig.savefig("%s.png" % prepath)
#     # # fig.draw()
#     # # fig.show()
#
#     ##########################################
#     # 3. Change gap from orbital to band basis
#     ##########################################
#     mh = KptMesh.read("%s/Input/calc.out" % filepath,
#                       [mhx, mhy, mhz], sgroup)
#     mh.sgroup.SetOrbs(["xy", "yz", "zx"])
#     proj = Projectors(norb, nband, mh.nkpt, nspin=nspin)
#     proj.read("%s/Input/forlb.ovlp" % filepath)
#     proj.orthonormalize()
#
#     mg = KptMesh.read("%s/Input/Sr2RuO4.klist" % filepath, [mgx, mgy, mgz],
#                       sgroup)
#     mg.sgroup.SetOrbs(orbs)
#
#     gap = Gap(ndim, mg, nfreq, nspin=nspin)
#     gap.Re.Read("%s/Input/Sr2RuO4.ReGapFn02" % filepath, _skip=1, _skipc=1)
#     gap.Im.Read("%s/Input/Sr2RuO4.ImGapFn02" % filepath, _skip=1, _skipc=1)
#
#     gap_b = gap.changeBasis(proj, mh)
#     gap_b.write("Sr2RuO4", prepath="%s/Output/3-" % filepath)
#
#     ####################################
#     # 4. Interpolate gap, print and plot
#     ####################################
#     # mi2 = KptMesh([mix, miy, miz], 139)
#     # gapi = gap_b.interpolate(mi2)
#     # gapi.write("Sr2RuO4", prepath="%s/Output/4-" % filepath)
#
#     # with open("%s/Output/4-gap.gap" % filepath, "w") as f:
#     #     with open("%s/Output/4-gap.klist" % filepath, "w") as fk:
#     #         fk.write("%s\n" % mi2.nkpt)
#     #         for k, kpt in enumerate(mi2.npts):
#     #             npt = mi2.fromNgetM(kpt)
#     #             fk.write("%.12f    %.12f    %.12f\n" %
#     #                      (npt[0], npt[1], npt[2]))
#     #             line = ""
#     #             for d, d2 in itp(range(nband), repeat=2):
#     #                 line += ("%.12f   %.12f   " %
#     #                          (gapi.Re[k][0][d][d2], gapi.Im[k][0][d][d2]))
#     #             f.write(line + "\n")
#
#     # Remove Output directory
#     shutil.rmtree("%s/Output" % filepath)


def test_bogo_03():
    filepath = "%s/test_bogo_03" % dirpath

    mhx, mhy, mhz = 15, 15, 2
    mgx, mgy, mgz = 5, 5, 2
    mix, miy, miz = 60, 60, 1
    mix, miy, miz = 120, 120, 1
    mix, miy, miz = 30, 30, 1
    sgroup = 139
    orbs = ["xy", "yz", "zx"]
    nspin, norb = 2, 3
    nfreq = 2
    fermi = 6.9432565
    broad = 0.01
    gap_norm = 4e-3
    omega = np.arange(-1e-3, 1e-3, 2e-5)

    post_path = "%s/PostProd" % filepath
    if os.path.exists(post_path):
        shutil.rmtree(post_path)
    os.makedirs(post_path)

    #######################################################
    # 1. Interpolate Hamiltonian
    # We start from the orbital and spin basis Hamiltonian.
    # First we project to the pseudospin basis and reduce
    # dimensionality. Then we interpolate and write.
    ######################################################
    ndim = nspin*norb
    mh = KptMesh.read("%s/Input/SRO_orb.klist" % filepath,
                      [mhx, mhy, mhz], sgroup)
    mh.sgroup.SetOrbs(orbs)

    h0 = Hk(mh.nkpt, ndim, mh)
    h0.read("%s/Input/SRO_orb.Hk" % filepath)

    A, B = [3, 1, 2, 0, 4, 5], [0, 1, 2, 3, 4, 5]
    h0[:, A, :] = h0[:, B, :]
    h0[:, :, A] = h0[:, :, B]
    h0_uu = Hk(mh.nkpt, norb, mh)
    h0_dd = Hk(mh.nkpt, norb, mh)
    h0_uu[:] = h0[:, :norb, :norb]
    h0_dd[:] = h0[:, norb:, norb:]

    mi_huu = KptMesh([mix, miy, miz], 139)
    mi_hdd = KptMesh([mix, miy, miz], 139)
    h0i_uu = h0_uu.interpolate(mi_huu)
    h0i_dd = h0_dd.interpolate(mi_hdd)
    prepath = "%s/2-inter" % post_path
    h0i_uu.write(hk_out="%s_uu.Hk" % prepath,
                 klist_out="%s_uu.klist" % prepath)
    h0i_dd.write(hk_out="%s_dd.Hk" % prepath,
                 klist_out="%s_dd.klist" % prepath)

    mi = KptMesh.read("%s_uu.klist" % prepath, [mix, miy, miz], sgroup)

    # Plot
    mu = (fermi + 1j*broad)*np.identity(norb)

    G = np.zeros((mi.nkpt, norb, norb), dtype=complex)
    A = np.zeros((norb, 2*mix, 2*miy))
    for k, kpt in enumerate(mi.npts):
        G[k] = ln.inv(mu - h0i_uu[k])
        for d in range(norb):
            if kpt[2] == 0:
                A[d][kpt[0]][kpt[1]] = - 1./math.pi*np.imag(G[k][d][d])

    fig, ax = plt.subplots(1, norb, figsize=(16, 4))
    cutoff = [0 for i in range(norb)]
    for d in range(norb):
        mask = np.ma.masked_where(A[d] < cutoff[d], A[d])
        ax[d].imshow(mask, origin="lower")
    fig.savefig("%s.png" % prepath)
    # fig.draw()
    fig.show()

    ###################################################################
    # 3. Interpolate Gap
    # We load the gap, project it to an irreducible representation
    # (it already is quite projected, but this reduces numerical errors)
    # and project it to the pseudospin basis as well. We take only the
    # pseudospin-singlet solution. We then interpolate and write.
    ###################################################################
    mh = KptMesh.read("%s/Input/SRO_orb.klist" % filepath,
                      [mhx, mhy, mhz], sgroup)
    mh.sgroup.SetOrbs(orbs)

    mg = KptMesh.read("%s/Input/Sr2RuO4.klist" % filepath,
                      [mgx, mgy, mgz], sgroup)
    mg.sgroup.SetOrbs(orbs)

    gap = Gap(ndim, mg, nfreq, nspin=nspin)
    gap.Re.Read("%s/Input/Sr2RuO4.ReGapFn01" % filepath, _skip=1, _skipc=1)
    gap.Im.Read("%s/Input/Sr2RuO4.ImGapFn01" % filepath, _skip=1, _skipc=1)
    gap = gap.IrrepProjection(2)

    A, B = [3, 1, 2, 0, 4, 5], [0, 1, 2, 3, 4, 5]
    gap.Re.Obj[:, :, A, :] = gap.Re.Obj[:, :, B, :]
    gap.Re.Obj[:, :, :, A] = gap.Re.Obj[:, :, :, B]
    gap.Im.Obj[:, :, A, :] = gap.Im.Obj[:, :, B, :]
    gap.Im.Obj[:, :, :, A] = gap.Im.Obj[:, :, :, B]

    gap_ud = Gap(norb, mg, nfreq, nspin=1)
    gap_ud[:] = gap_norm*gap[:, :, :norb, norb:]
    gap_du = Gap(norb, mg, nfreq, nspin=1)
    gap_du[:] = gap_norm*gap[:, :, :norb, norb:]

    mi_gud = KptMesh([mix, miy, miz], sgroup)
    mi_gud.sgroup.SetOrbs(orbs)
    gapi_ud = gap_ud.interpolate(mi_gud)
    gapi_ud.write("Sr2RuO4_ud", prepath="%s/4-" % post_path)

    mi_gdu = KptMesh([mix, miy, miz], sgroup)
    mi_gdu.sgroup.SetOrbs(orbs)
    gapi_du = gap_du.interpolate(mi_gdu)
    gapi_du.write("Sr2RuO4_du", prepath="%s/4-" % post_path)

    gap2 = np.zeros((norb, norb, 2*mix, 2*miy), dtype=complex)
    for d1, d2 in itp(range(norb), repeat=2):
        for k, kpt in enumerate(mi_gud.npts):
            if kpt[2] == miz-1:
                re_gap = 0.5*(gapi_ud.Re.Obj[k][0][d1][d2] +
                              gapi_ud.Re.Obj[k][1][d1][d2])
                im_gap = 0.5*(gapi_ud.Im.Obj[k][0][d1][d2] +
                              gapi_ud.Im.Obj[k][1][d1][d2])
                gap2[d1][d2][kpt[0]][kpt[1]] = re_gap + 1j*im_gap

    fig, ax = plt.subplots(norb, norb, figsize=(16, 16))
    for d1, d2 in itp(range(norb), repeat=2):
        ax[d1][d2].imshow(gap2[d1][d2].real, origin="lower",
                          cmap=plt.get_cmap("bwr"))
    fig.savefig("%s/4-gapi.png" % post_path)
    fig.show()

    ###############################################################
    # 4. Bogoliubov at Fermi level
    # Construct the Bogoliubov hamiltonian with the gap
    # function. Compute the Bogoliubov Green functions and use
    # them to plot the spectral function at the Fermi level.
    ###############################################################

    bogo = Bogoliubov(h0i_uu, h0i_dd, gapi_ud, gapi_du, mesh=mi)
    bogo.construct_ham(fermi)

    g_FS = bogo.return_green([0], broad=broad)

    A_FS = np.zeros((2*norb, 2*mix, 2*miy))
    s3 = np.kron(np.diag([1, -1]), np.eye(norb))
    for k, kpt in enumerate(mi_gud.npts):
        kx, ky, kz = kpt[0], kpt[1], kpt[2]
        re_gap = 0.5*(gapi_ud.Re.Obj[k][0]+gapi_ud.Re.Obj[k][1])
        im_gap = 0.5*(gapi_ud.Im.Obj[k][0]+gapi_ud.Im.Obj[k][1])
        if kz == miz-1:
            gap2[:, :, kx, ky] = re_gap + 1j*im_gap
            s3g = s3*g_FS[k, 0]
            for d in range(norb):
                d2 = d+norb
                A_FS[d, kx, ky] = -1./mi.nkpt/math.pi*np.imag(s3g)[d, d]
                s3g_temp = np.imag(np.conj(np.transpose(s3g)))
                A_FS[d+norb, kx, ky] = -1./mi.nkpt/math.pi*s3g_temp[d2, d2]

    fig, ax = plt.subplots(norb, norb, figsize=(16, 16))
    for d1, d2 in itp(range(norb), repeat=2):
        ax[d1][d2].imshow(gap2[d1][d2].real, origin="lower",
                          cmap=plt.get_cmap("bwr"))
    fig.savefig("%s/5-Gap%d.png" % (post_path, mix))
    fig.show()

    fig, ax = plt.subplots(2, norb, figsize=(16, 8))
    cutoff = [0 for i in range(2*norb)]
    for d in range(norb):
        ax[0][d].imshow(A_FS[d], origin="lower")
        ax[1][d].imshow(A_FS[d+norb], origin="lower")
    fig.savefig("%s/5-spectral_partial%d.png" % (post_path, mix))
    fig.show()

    fig = plt.figure(figsize=(12, 12))
    image = plt.imshow(np.sum(A_FS, axis=0))
    plt.colorbar(image)
    fig.savefig("%s/5-spectral%d.png" % (post_path, mix))
    fig.show()

    ######################################
    # 5. Bogoliubov on real frequency mesh
    ######################################
    bogo0 = Bogoliubov(h0i_uu, h0i_dd, None, None, mesh=mi)
    bogo0.construct_ham(fermi)

    fig, ax = plt.subplots(5, 1, figsize=(8, 12))
    for b, broad in enumerate([0.1, 0.01, 0.001, 0.0001, 0.00001]):
        g0_w = bogo0.return_green(omega, broad=broad)
        g_w = bogo.return_green(omega, broad=broad)
        nw = len(omega)
        A0_w = np.zeros((nw))
        A_w = np.zeros((nw))
        for w in range(nw):
            A0_temp = np.zeros((2*norb, 2*mix, 2*miy, 2*miz))
            A_temp = np.zeros((2*norb, 2*mix, 2*miy, 2*miz))
            for k, kpt in enumerate(mi.npts):
                kx, ky, kz = kpt[0], kpt[1], kpt[2]
                s3g0 = s3*g0_w[k, w]
                s3g = s3*g_w[k, w]
                for d in range(norb):
                    d2 = d + norb
                    temp = -1./mi.nkpt/math.pi*np.imag(s3g0)[d, d]
                    A0_temp[d, kx, ky, kz] = temp
                    temp = (-1./mi.nkpt/math.pi *
                            np.imag(np.conj(np.transpose(s3g0)))[d2, d2])
                    A0_temp[d2, kx, ky, kz] = temp
                    temp = -1./mi.nkpt/math.pi*np.imag(s3g)[d, d]
                    A_temp[d, kx, ky, kz] = temp
                    temp = (-1./mi.nkpt/math.pi *
                            np.imag(np.conj(np.transpose(s3g)))[d2, d2])
                    A_temp[d2, kx, ky, kz] = temp
            A0_w[w] = np.sum(A0_temp)
            A_w[w] = np.sum(A_temp)

        ax[b].plot(omega, A0_w, linestyle="--")
        ax[b].plot(omega, A_w, label=str(broad))
    plt.legend()
    plt.savefig("%s/6-STM%d.png" % (post_path, mix))
    plt.show()

    # Remove Output directory
    shutil.rmtree("%s" % post_path)
