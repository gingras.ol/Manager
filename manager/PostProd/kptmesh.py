import itertools
import warnings

from abisuite import AbinitOutputFile
import matplotlib.pyplot as plt
# used for the 3D projection
from mpl_toolkits.mplot3d import Axes3D  # noqa
import numpy as np
import scipy.linalg as ln

from .bases import PostProdBase
from .SGroup import SGroup


class KptMeshBase(PostProdBase):
    """Base class for all kptmesh objects.
    Here are regrouped the common methods for all those objects.
    """

    def __init__(self, sgroup, **kwargs):
        """Common init method for a kptmesh object.

        Needs the space group number to set as an attribute.

        Parameters
        ----------
        sgroup : int
                 Space group number.
        """
        super().__init__(**kwargs)
        self._init_SGroup_matrices(sgroup)

    @classmethod
    def _extract_kpts_from_file(cls, _file, _type=None, **kwargs):
        if _type is None:
            if _file.endswith(".out"):
                _type = "out"
            elif _file.endswith(".klist"):
                _type = "klist"
            elif _file.endswith(".kmesh"):
                _type = "kmesh"
            else:
                raise ValueError("This type of file is not recongnised. Please"
                                 " specify the type.")
        # Reading depends on the file type
        if _type == 'kmesh':
            # Type resulting from KptMesh.KeepMesh
            raise NotImplementedError('_type = kmesh: to do.')
        elif _type == 'klist':
            # Type of file ending with klist or qlist
            kpts, nkpt = cls._get_kpts_from_klist(_file)
        elif _type == "out":
            kpts = cls._get_kpts_from_outfile(_file, **kwargs)
            nkpt = len(kpts)
        else:
            raise ValueError("No type specified.")
        return kpts, nkpt

    @staticmethod
    def _get_kpts_from_klist(_file):
        kpts = []
        with open(_file, 'r') as f:
            lines = f.readlines()
        for i, line in enumerate(lines):
            if i == 0:
                nkpt = int(line.split()[0])
                continue
            m0 = float(line.split()[0])
            m1 = float(line.split()[1])
            m2 = float(line.split()[2])
            kpts.append([m0, m1, m2])
        return kpts, nkpt

    @staticmethod
    def _get_kpts_from_outfile(_file, dataset=None, logger=None):
        # OUT file from ABINIT
        with AbinitOutputFile.from_file(_file) as out:
            print(out.input_variables.keys())
            # get kpts string
            if len(out.dtsets) > 1:
                if dataset is None:
                    raise ValueError("Need to specify dataset if more than "
                                     "one.")
                kpts = out.input_variables[f"kpt{dataset}"]
            else:
                kpts = out.input_variables["kpt"]
            # if prtvol != 3, not all kpts may be printed
            prtvol = out.input_variables["prtvol"]
        if prtvol not in [3, 4]:
            msg = f"prtvol = {prtvol} != 3 or 4; all kpts may not be written."
            warnings.warn(msg)
        # transform kpts string to kpt array
        kpts = np.array(kpts, dtype=float)
        # mirror points
        # where = np.where(np.logical_and(kpts[:, 1] == 0.5,
        #                                kpts[:, 2] == 0.5))[0]
        # kpts[where][:, 1] = -0.5
        return kpts

    def _init_SGroup_matrices(self, sgroup):
        """Defines the transformation matrices to map a kpt from the reduced
        basis to the cartesian reciprocal basis, depending of the space group.

        Parameters
        ----------
        sgroup : int
                 The space group number.
        """
        if sgroup in [23, 71, 139, 97, 'cube']:
            self.sgroup = SGroup(sgroup)
        else:
            raise NotImplementedError('space group %s is not implemented.' %
                                      sgroup)
        self._logger.info('KptMesh.SGroup: sgroup is now %s' %
                          str(self.sgroup.sgroup))

    def __getitem__(self, key):
        return self.mesh[key]

    def __setitem__(self, key, value):
        self.mesh[key] = value

    def _get_data(self):
        return self.mesh


class KptMesh1D(KptMeshBase):
    _loggername = "postprod.kptmesh1D"

    def __init__(self, kpts, *args, **kwargs):
        """1D kptmesh object init method. Designed for a simple list of kpts
        instead of a complex 3D grid.

        Parameters
        ----------
        kpts : array
               List of kpts coordinates.
        """
        super().__init__(*args, **kwargs)
        self.kpts = kpts
        self.nkpt = len(kpts)
        self.mesh = np.array(range(self.nkpt))

    @classmethod
    def read(cls, filepath, sgroup, **kwargs):
        kpts, nkpt = cls._extract_kpts_from_file(filepath,
                                                 _type=kwargs.pop("_type",
                                                                  None))
        return cls(kpts, sgroup, **kwargs)


class KptMesh(KptMeshBase):
    _loggername = "postprod.kptmesh"

    def __init__(self, n, sgroup, fullspan=True, **kwargs):
        """Object that represents a kpt mesh. Used to manipulate kpts
        intuitively.

        Parameters
        ----------
        n : array-like
            Dimensions of the kpt mesh in each reciprocal space directions.
        sgroup : int
                 Space group number.
        fullspan : bool, optional
                   If True, the given kptmesh will be defined for a mesh
                   spanning the whole BZ. For example, given kpts on the
                   Gamma - X line, the kptmesh will be defined for a
                   path -X - Gamma - X. Samething for other dimensions.
                   Essentially, this will double the size of the kptmesh
                   in every directions for certain group symmetries.

        Example
        -------
        # Create kpt grid tensor
        >>> mesh = KptMesh([40, 40, 1], 139)
        # fill the mesh from a list of kpts.
        >>> mesh.fill_mesh(kpts)
        # create kptmesh from a file
        >>> mesh = KptMesh.read("calc.out", [40, 40, 1], 139,
                                _type="out", nkpt=41)
        # get the indices of the mesh along a path that starts at the center
        >>> kpts = mesh[39:, 39, 0]
        """
        super().__init__(sgroup, **kwargs)
        if len(n) != 3:
            raise ValueError('The argument must be 3d.')
        self.n = n
        isint = [isinstance(x, int) for x in self.n]
        if not all(isint):
            raise TypeError("The kpt mesh dimensions must be integers.")

        # Inits the matrices used to change between basis
        self.S = np.diag([self.n[0], self.n[1], self.n[2]])

        # Inits parameters
        self.npts = []
        shape = np.array(self.n)
        self.kpts = None
        # Inits the mesh
        if self.sgroup.sgroup in [23, 71, 97, 139]:
            self.N0 = np.transpose(np.matrix(shape - 1))

            if fullspan:
                shape *= 2
            self.mesh = -1 * np.ones(shape,
                                     dtype=int)
        elif self.sgroup.sgroup == "cube":
            self.mesh = -1 * np.ones(shape,
                                     dtype=int)
            self.N0 = np.transpose(np.matrix(shape/2 - 1))

    def fill_mesh(self, kpts):
        """From a list of kpts in reduced coordinates, apply symmetries
        from the SGroup to fill the kpt mesh.

        Parameters
        ----------
        kpts : The list of kpts to fill the mesh from. The size of the mesh
               should be compatible with the size of this list. If not, an
               error will be raised.
        """
        self.kpts = kpts
        self.nkpt = len(kpts)
        for i, k in enumerate(kpts):
            m = np.transpose(np.matrix(k))
            n = self.S * self.sgroup.B * m + self.N0
            n = self.translate_in_FBZ([n.item(0), n.item(1), n.item(2)])
            if self[round(n[0]), round(n[1]), round(n[2])] != -1:
                raise ValueError("Trying to"
                                 " fill, kpt_mesh is not -1.")
            self[round(n[0]), round(n[1]), round(n[2])] = i
            self.npts.append([round(n[0]), round(n[1]), round(n[2])])
        self._logger.info("KptMesh: %d kpts could be filled"
                          " from the file." % self.nkpt)

    def plot_kpts(self):
        """Plot the kpts in a 3D map (in reduced coordinates).
        """
        fig = plt.figure()
        xs = self.kpts[:, 0]
        ys = self.kpts[:, 1]
        zs = self.kpts[:, 2]
        # if one axis is only 1 in length, just plot a 2d map
        u_x = np.unique(xs)
        u_y = np.unique(ys)
        u_z = np.unique(zs)
        if len(u_x) == 1 or len(u_y) == 1 or len(u_z) == 1:
            ax = fig.add_subplot(111)
            # only 2D map, plot it!
            if len(u_x) == 1:
                # kx-k_y plane
                ax.set_xlabel(r"$k_y$")
                ax.set_ylabel(r"$k_z$")
                ax.set_title(r"$k_x=$%f" % u_x[0])
                ax.scatter(ys, zs, color="k", marker="o")
            elif len(u_y) == 1:
                # kx-k_z plane
                ax.set_xlabel(r"$k_x$")
                ax.set_ylabel(r"$k_z$")
                ax.set_title(r"$k_y=$%f" % u_y[0])
                ax.scatter(xs, zs, color="k", marker="o")
            elif len(u_z) == 1:
                # kx-k_y plane
                ax.set_xlabel(r"$k_x$")
                ax.set_ylabel(r"$k_y$")
                ax.set_title(r"$k_z=$%f" % u_z[0])
                ax.scatter(xs, ys, color="k", marker="o")
        else:
            ax = fig.add_subplot(111, projection="3d")
            ax.scatter(xs, ys, zs, color="k", marker="o")
            ax.set_xlabel(r"$k_x$")
            ax.set_ylabel(r"$k_y$")
            ax.set_zlabel(r"$k_z$")
        plt.show()

    def fromNgetM(self, _n):
        mat = np.transpose(np.matrix((_n[0], _n[1], _n[2])))
        return ln.inv(self.sgroup.B).dot(ln.inv(self.S).dot(mat - self.N0))

    def fromMgetN(self, _m):
        mat = np.transpose(np.matrix((_m[0], _m[1], _m[2])))
        out = self.S.dot(self.sgroup.B.dot(mat)) + self.N0
        return np.transpose(np.matrix((round(out.item(0)), round(out.item(1)),
                                       round(out.item(2)))))

    def in_FBZ(self, m):
        """Checks whether a point in reciprocal space is in the First BZ."""
        half = np.transpose(np.matrix((0.5, 0.5, 0.5)))
        if (m - half < 1e-10).all() and (m + half > 1e-10).all():
            return True
        else:
            return False

    def translate_in_FBZ(self, _n):
        """Given a kpt in the tensor notation, returns the corresponding point
           in the First Brillouin Zone.
        Parameters
        ----------
        _n : array-like
             Tensor index of the point.
        """
        # Write tensor index point in reciprocal space bases
        k = self.fromNgetM(_n)
        # If in FBZ, return the given
        if self.in_FBZ(k):
            return _n

        # List of translational vectors in tensor basis
        T = [self.fromMgetN([1, 0, 0]) - self.N0,
             self.fromMgetN([0, 1, 0]) - self.N0,
             self.fromMgetN([0, 0, 1]) - self.N0]

        # Test all translational combinaison and return the point in FBZ
        for i in range(2):
            if not i:
                r = range(-1, 2)
            else:
                # Rarely, one translational vector is not enough. It is so rare
                # that I decided to code it like this.
                r = range(-2, 3)
            for t0, t1, t2 in itertools.product(r, r, r):
                new_n = np.transpose(np.matrix((_n[0], _n[1], _n[2])))
                new_n += t0*T[0] + t1*T[1] + t2*T[2]
                new_n = [round(new_n.item(0)), round(new_n.item(1)),
                         round(new_n.item(2))]
                k = self.fromNgetM(new_n)
                if self.in_FBZ(k):
                    return new_n

        # If no point found, error
        raise ValueError("This n point has no near point in the FBZ: ", _n)

    @classmethod
    def read(cls, _file, *args, _type=None, nkpt=None, dataset=None,
             **kwargs):
        """Get the kpt mesh from a file.

        Parameters
        ----------
        _file : str
                The file path.
        _type : str, {'out', 'klist', 'kmesh'}
                The file type.
        nkpt : int, optional
               If not None, checks that the number of kpts read from file
               is consistent with this number.

        Other args go to the init method. See KptMesh.__init__
        """
        check_nkpt = nkpt
        kpts, nkpt = cls._extract_kpts_from_file(_file, _type, dataset=dataset)
        if check_nkpt is not None:
            assert check_nkpt == nkpt
        mesh = cls(*args, **kwargs)
        mesh.fill_mesh(kpts)
        return mesh

    def write(self, _type, _file):
        """From a filled, write the k points in _file._type.

        Parameters
        ----------
        _type : string
                Type of written file. For now only implemented as klist.
        _file : string
                Written filename.
        """
        if not self.nkpt:
            raise ValueError("Mesh is empty. Nothing to write.")

        with open(_file, 'w') as f:
            if _type == "klist":
                f.write("%s\n" % self.nkpt)
                for k in range(self.nkpt):
                    kpt = self.fromNgetM(self.npts[k])
                    f.write("%.12f %.12f %.12f\n" % (kpt[0], kpt[1], kpt[2]))
            else:
                raise NotImplementedError("This type is not implemented.")

    def getSubmesh(self, n):
        """Get a submesh from the full mesh.

        Parameters
        ----------
        n : array-like
            The number of kpts in each reciprocal space directions for the
            submesh.
        """
        mod = 0
        for i in range(3):
            mod += self.n[i] % n[i]

        if mod != 0:
            raise ValueError("Submesh dimensions is not compatible with the"
                             " full mesh.")
        subMesh = KptMesh(n, self.sgroup.sgroup)

        nkpt = 0
        for sx, sy, sz in itertools.product(range(2*n[0]), range(2*n[1]),
                                            range(2*n[2])):
            kx = (sx - subMesh.N0.item(0)) * self.n[0] / n[0] + self.N0.item(0)
            ky = (sy - subMesh.N0.item(1)) * self.n[1] / n[1] + self.N0.item(1)
            kz = (sz - subMesh.N0.item(2)) * self.n[2] / n[2] + self.N0.item(2)
            m = self[round(kx), round(ky), round(kz)]
            subMesh[sx, sy, sz] = m
            if m != -1:
                kpt = subMesh.fromNgetM([sx, sy, sz])
                kpt = [kpt.item(0), kpt.item(1), kpt.item(2)]
                npt = [sx, sy, sz]
                subMesh.npts.append(npt)
                nkpt += 1
        subMesh.nkpt = nkpt
        self._logger.info("ReturnSubmesh: Done filling the submesh with " +
                          str(subMesh.nkpt) + " k points.")
        return subMesh

    def getSubpath(self, qpath, out_name="out.qlist", prt_qpath=False):
        """Get a path from the kptmesh.

        Parameters
        ----------
        qpath : array-like
                Array of the q points forming the path.
        """
        qlist = []
        qpt = 0

        if not len(qpath):
            raise ValueError("Empty q-path is useless.")
        if len(qpath) == 1:
            qlist.append(self[qpath[0]])
        else:
            for i in range(len(qpath)-1):
                dist = max(abs(qpath[i+1][0]-qpath[i][0]),
                           abs(qpath[i+1][1]-qpath[i][1]),
                           abs(qpath[i+1][2]-qpath[i][2]))
                print("i = %d; max = %d" % (i, dist))
                for j in range(dist):
                    nx = int(qpath[i][0] + 1. * j / dist *
                             (qpath[i + 1][0] - qpath[i][0]))
                    ny = int(qpath[i][1] + 1. * j / dist *
                             (qpath[i + 1][1] - qpath[i][1]))
                    nz = int(qpath[i][2] + 1. * j / dist *
                             (qpath[i + 1][2] - qpath[i][2]))
                    if prt_qpath:
                        print("Qpt %d; nx, ny, nz = %d %d %d; kpt =" %
                              (qpt, nx, ny, nz), self.kpts[self[nx, ny, nz]])
                        qpt += 1
                    qlist.append(self.kpts[self[nx, ny, nz]])
            nx = qpath[-1][0]
            ny = qpath[-1][1]
            nz = qpath[-1][2]
            if prt_qpath:
                print("Qpt %d; nx, ny, nz = %d %d %d; kpt =" %
                      (qpt, nx, ny, nz), self.kpts[self[nx, ny, nz]])
                qpt += 1
            qlist.append(self.kpts[self[nx, ny, nz]])

        np.savetxt(out_name, qlist)
        with open(out_name, 'r') as original:
            data = original.read()
        with open(out_name, 'w') as modified:
            modified.write(str(len(qlist))+'\n' + data)
