from .physobj import PhysObj
from .kptmesh import KptMesh
from .plotting import PlotParams, setFigure, plotGrid2, setSubplot, finish
import numpy as np
import scipy.linalg as ln
from itertools import product as itp
from scipy.interpolate import RegularGridInterpolator as GridInt
import regex as re
import cmath
import math


class Gap:
    """
    The gap function can be manipulated for various calculations.
    """
    def __init__(self, ndim, mesh, nfreq, nspin=1):
        print("Gap.__init__: Initializing Gap function object.")

        if not isinstance(mesh, KptMesh):
            raise TypeError("ERROR in Gap.__init__: mesh has to be "
                            "a KptMesh object.")
        elif not mesh.nkpt:
            raise ValueError("ERROR in Gap.__init__: mesh has no kpoints.")
        elif not ndim:
            raise ValueError("ERROR in Gap.__init__: ndim cannot be 0.")
        elif not nfreq:
            raise ValueError("ERROR in Gap.__init__: nfreq cannot be 0.")
        elif nfreq % 2:
            raise ValueError("Gap must have an even number of fermionic "
                             "frequencies, since it's defined on both "
                             "positive and negative frequencies.")

        self.ndim = ndim
        self.mesh = mesh
        self.nfreq = nfreq
        self.nspin = nspin

        self.Re = PhysObj(ndim, mesh.nkpt, nfreq, linear=True)
        self.Im = PhysObj(ndim, mesh.nkpt, nfreq, linear=True)

    def write(self, system, prepath=""):
        """
        Useful for bogoliubov.
        """
        mesh = self.mesh
        w0 = int(self.nfreq/2) - 1
        with open("%s%s.Re" % (prepath, system), "w") as file_re, \
                open("%s%s.Im" % (prepath, system), "w") as file_im, \
                open("%s%s.klist" % (prepath, system), "w") as file_k:
            file_re.write("%d\t%d\n" % (self.mesh.nkpt, self.nfreq))
            file_im.write("%d\t%d\n" % (self.mesh.nkpt, self.nfreq))
            file_k.write("%d\n" % self.mesh.nkpt)
            for k, kpt in enumerate(mesh.npts):
                npt = mesh.fromNgetM(kpt)
                file_k.write("%.12f\t%.12f\t%.12f\n" %
                             (npt[0], npt[1], npt[2]))
                line_re1, line_re2 = "%d\t" % k, "%d\t" % k
                line_im1, line_im2 = "%d\t" % k, "%d\t" % k
                for d1, d2 in itp(range(self.ndim), range(self.ndim)):
                    line_re1 += "%.12f\t" % self.Re[k][w0][d1][d2]
                    line_re2 += "%.12f\t" % self.Re[k][w0+1][d1][d2]
                    line_im1 += "%.12f\t" % self.Im[k][w0][d1][d2]
                    line_im2 += "%.12f\t" % self.Im[k][w0+1][d1][d2]
                file_re.write(line_re1 + "\n")
                file_re.write(line_re2 + "\n")
                file_im.write(line_im1 + "\n")
                file_im.write(line_im2 + "\n")

    def PlotEliashGap(self, params=None):
        if params is None:
            params = PlotParams()
        elif not isinstance(params, PlotParams):
            raise TypeError("Error: params must be of type PlotParams.")

        # ndim = self.ndim
        nfreq, m = self.nfreq, self.mesh

        default = PlotParams()
        default["subplots"] = [2, 2]
        default["indices"] = [[[0, 0], [1, 1]] for i in range(2)]
        default["freqs"] = range(int(nfreq/2))
        default["figsize"] = (12, 8)
        default["title"] = ""
        default["titlesize"] = 14
        xlim, ylim = [0, 2*m.n[0]-1], [0, 2*m.n[1]]
        default["xlim"] = [[xlim, xlim], [xlim, xlim]]
        default["ylim"] = [[ylim, ylim], [ylim, ylim]]
        xlabels = [r"$\Im\{\Delta_{[&l1],[&l2]}(\vec{q},\omega_{[&w]})\}$",
                   r"$\Im\{\Delta_{[&l1],[&l2]}(\vec{q},\omega_{[&w]})\}$"]
        default["xlabels"] = [[xlabels, xlabels], [xlabels, xlabels]]
        default["ylabels"] = None
        default["xticks"] = [None]
        default["yticks"] = [None]
        default["ticksize"] = 10
        default["cmap"] = [["bwr", "bwr"], ["bwr", "bwr"]]
        default["bar_format"] = [["%01.01e", "%01.01e"],
                                 ["%01.01e", "%01.01e"]]
        default["bar_ticksize"] = 10
        default["tight_layout"] = True
        default["show"] = False
        default["savefig"] = None
        default["special1"] = False
        default["special2"] = False
        default["sharey"] = False

        params.setDefaults(default)

        prtws = []
        for w in range(-int(nfreq/2) + 1, 0):
            prtws.append(str(w))
        prtws.append("-0")
        for w in range(int(nfreq/2)):
            prtws.append(str(w))

        for v in params["freqs"]:
            fig, ax = setFigure(params)

            wp = int(nfreq/2) + v
            wm = int(nfreq/2) - 1 - v

            for b in range(params["subplots"][1]):
                l1 = params["labels"][params["indices"][0][b][0]]
                l2 = params["labels"][params["indices"][0][b][1]]
                print(params["indices"])
                printVariables = {"l1": r"%s" % l1, "l2": r"%s" % l2,
                                  "w": prtws[wp]}
                plotGrid2(ax, 0, b, [self.Re, self.Im],
                          wp, m, params, printVariables)
                l1 = params["labels"][params["indices"][1][b][0]]
                l2 = params["labels"][params["indices"][1][b][1]]
                printVariables["w"] = prtws[wm]
                plotGrid2(ax, 1, b, [self.Re, self.Im],
                          wm, m, params, printVariables)

            setSubplot(ax, params)

            if params["savefig"] is None:
                saveplace = None
            else:
                saveplace = params["savefig"]
            finish(params, saveplace=saveplace)

    def SPOT_contributions(self, analysis=False):
        """
        From a given gap, seperate then different components in terms
        of even-ness/odd-ness vs SPOT (Spins, parity, orbitals and
        frequency (time).
        """
        gap = self
        nfreq, ndim, m = self.nfreq, self.ndim, self.mesh
        norb = int(ndim/2)
        mx, my, mz = m.n[0], m.n[1], m.n[2]
        gap2 = np.zeros((nfreq, ndim, ndim, 2*mx, 2*my, 2*mz), dtype=complex)

        for v, d1, d2 in itp(range(nfreq), range(ndim), range(ndim)):
            for k, kpt in enumerate(m.npts):
                kx, ky, kz = kpt[0], kpt[1], kpt[2]
                gap2[v][d1][d2][kx][ky][kz] = gap[k][v][d1][d2]
        print("Max of gap: \n\t", np.amax(np.linalg.norm(gap2)))

        # Time
        eo_T = np.zeros((2, nfreq, ndim, ndim, 2*mx, 2*my, 2*mz),
                        dtype=complex)

        eo_T2 = np.zeros((2), dtype=Gap)
        for T in range(2):
            eo_T2[T] = Gap(ndim, m, nfreq)

        for v in range(nfreq):
            v2 = int(-1*(v - (nfreq-1)/2) + (nfreq-1)/2)

            RIg = self.Re.Obj[:, v]
            RTg = self.Re.Obj[:, v2]
            eo_T2[0].Re.Obj[:, v] = .5*(RIg + RTg)
            eo_T2[1].Re.Obj[:, v] = .5*(RIg - RTg)

            IIg = self.Im.Obj[:, v]
            ITg = self.Im.Obj[:, v2]
            eo_T2[0].Im.Obj[:, v] = .5*(IIg + ITg)
            eo_T2[1].Im.Obj[:, v] = .5*(IIg - ITg)

        index_set = itp(range(nfreq), range(ndim), range(ndim),
                        range(2*mx), range(2*my), range(2*mz))
        for v, d1, d2, kx, ky, kz in index_set:
            v2 = -1*(v - (nfreq-1)/2) + (nfreq-1)/2
            v2 = int(v2)

            l1, s1 = int(d1 % 3), int(d1/3)
            l2, s2 = int(d2 % 3), int(d2/3)

            Ig = gap2[v][d1][d2][kx][ky][kz]
            Tg = gap2[v2][d1][d2][kx][ky][kz]
            eo_T[0][v][d1][d2][kx][ky][kz] = 0.5*(Ig + Tg)
            eo_T[1][v][d1][d2][kx][ky][kz] = 0.5*(Ig - Tg)
        # print("Time max: ", np.amax(np.linalg.norm(eo_T)))

        # Orbitals
        eo_OT = np.zeros((2, 2, nfreq, ndim, ndim, 2*mx, 2*my, 2*mz),
                         dtype=complex)

        eo_OT2 = np.zeros((2, 2), dtype=Gap)
        for O, T in itp(range(2), repeat=2):
            eo_OT2[O, T] = Gap(ndim, m, nfreq)

        for d1, d2 in itp(range(ndim), repeat=2):
            l1, s1 = int(d1 % 3), int(d1/3)
            l2, s2 = int(d2 % 3), int(d2/3)
            dd1, dd2 = s1*norb+l2, s2*norb+l1

            for T in range(2):
                RIg = eo_T2[T].Re.Obj[:, :, d1, d2]
                ROg = eo_T2[T].Re.Obj[:, :, dd1, dd2]
                eo_OT2[0, T].Re.Obj[:, :, d1, d2] = .5*(RIg + ROg)
                eo_OT2[1, T].Re.Obj[:, :, d1, d2] = .5*(RIg - ROg)

                IIg = eo_T2[T].Im.Obj[:, :, d1, d2]
                IOg = eo_T2[T].Im.Obj[:, :, dd1, dd2]
                eo_OT2[0, T].Im.Obj[:, :, d1, d2] = .5*(IIg + IOg)
                eo_OT2[1, T].Im.Obj[:, :, d1, d2] = .5*(IIg - IOg)

        index_set = itp(range(nfreq), range(ndim), range(ndim),
                        range(2*mx), range(2*my), range(2*mz))
        for v, d1, d2, kx, ky, kz in index_set:
            l1, s1 = int(d1 % 3), int(d1/3)
            l2, s2 = int(d2 % 3), int(d2/3)

            for T in range(2):
                Ig = eo_T[T][v][d1][d2][kx][ky][kz]
                Og = eo_T[T][v][s1*3+l2][s2*3+l1][kx][ky][kz]
                eo_OT[0][T][v][d1][d2][kx][ky][kz] = 0.5*(Ig + Og)
                eo_OT[1][T][v][d1][d2][kx][ky][kz] = 0.5*(Ig - Og)
        # print("Orb Time max: ", np.amax(np.linalg.norm(eo_OT)))

        # Parity
        eo_POT = np.zeros((2, 2, 2, nfreq, ndim, ndim, 2*mx, 2*my, 2*mz),
                          dtype=complex)

        eo_POT2 = np.zeros((2, 2, 2), dtype=Gap)
        for P, O, T in itp(range(2), repeat=3):
            eo_POT2[P, O, T] = Gap(ndim, m, nfreq)

        for k, kpt in enumerate(m.npts):
            kx, ky, kz = kpt[0], kpt[1], kpt[2]
            n1 = np.transpose(np.matrix([kx, ky, kz]))
            k1 = n1 - m.N0
            k2 = -1*k1
            n2 = k2 + m.N0
            n2 = m.translate_in_FBZ([n2.item(0), n2.item(1), n2.item(2)])
            k2x, k2y, k2z = (int(round(n2[0])), int(round(n2[1])),
                             int(round(n2[2])))
            k2 = m[k2x, k2y, k2z]

            for O, T in itp(range(2), repeat=2):
                RIg = eo_OT2[O, T].Re.Obj[k]
                RPg = eo_OT2[O, T].Re.Obj[k2]
                eo_POT2[0, O, T].Re.Obj[k] = .5*(RIg + RPg)
                eo_POT2[1, O, T].Re.Obj[k2] = .5*(RIg - RPg)

                IIg = eo_OT2[O, T].Im.Obj[k]
                IPg = eo_OT2[O, T].Im.Obj[k2]
                eo_POT2[0, O, T].Im.Obj[k] = .5*(IIg + IPg)
                eo_POT2[1, O, T].Im.Obj[k2] = .5*(IIg - IPg)

        index_set = itp(range(nfreq), range(ndim), range(ndim),
                        range(2*mx), range(2*my), range(2*mz))
        for v, d1, d2, kx, ky, kz in index_set:
            if m.in_FBZ(m.fromNgetM([kx, ky, kz])):
                n1 = np.transpose(np.matrix([kx, ky, kz]))
                k1 = n1 - m.N0
                k2 = -1*k1
                n2 = k2 + m.N0
                n2 = m.translate_in_FBZ([n2.item(0), n2.item(1), n2.item(2)])
                k2x, k2y, k2z = (int(round(n2[0])), int(round(n2[1])),
                                 int(round(n2[2])))

                for O, T in itp(range(2), repeat=2):
                    Ig = eo_OT[O][T][v][d1][d2][kx][ky][kz]
                    Pg = eo_OT[O][T][v][d1][d2][k2x][k2y][k2z]
                    eo_POT[0][O][T][v][d1][d2][kx][ky][kz] = 0.5*(Ig + Pg)
                    eo_POT[1][O][T][v][d1][d2][kx][ky][kz] = 0.5*(Ig - Pg)
        # print("Parity Orb Time max: ", np.amax(np.linalg.norm(eo_POT)))

        # Spins
        eo_SPOT = np.zeros((2, 2, 2, 2, nfreq, ndim, ndim, 2*mx, 2*my, 2*mz),
                           dtype=complex)

        eo_SPOT2 = np.zeros((2, 2, 2, 2), dtype=Gap)
        for S, P, O, T in itp(range(2), repeat=4):
            eo_SPOT2[S, P, O, T] = Gap(ndim, m, nfreq, nspin=self.nspin)

        for d1, d2 in itp(range(ndim), repeat=2):
            l1, s1 = int(d1 % 3), int(d1/3)
            l2, s2 = int(d2 % 3), int(d2/3)
            dd1, dd2 = s2*norb+l1, s1*norb+l2

            for P, O, T in itp(range(2), repeat=3):
                RIg = eo_POT2[P, O, T].Re.Obj[:, :, d1, d2]
                RSg = eo_POT2[P, O, T].Re.Obj[:, :, dd1, dd2]
                eo_SPOT2[0, P, O, T].Re.Obj[:, :, d1, d2] = .5*(RIg + RSg)
                eo_SPOT2[1, P, O, T].Re.Obj[:, :, d1, d2] = .5*(RIg - RSg)

                IIg = eo_POT2[P, O, T].Im.Obj[:, :, d1, d2]
                ISg = eo_POT2[P, O, T].Im.Obj[:, :, dd1, dd2]
                eo_SPOT2[0, P, O, T].Im.Obj[:, :, d1, d2] = .5*(IIg + ISg)
                eo_SPOT2[1, P, O, T].Im.Obj[:, :, d1, d2] = .5*(IIg - ISg)

        index_set = itp(range(nfreq), range(ndim), range(ndim),
                        range(2*mx), range(2*my), range(2*mz))
        for v, d1, d2, kx, ky, kz in index_set:
            l1, s1 = int(d1 % 3), int(d1/3)
            l2, s2 = int(d2 % 3), int(d2/3)

            for P, O, T in itp(range(2), repeat=3):
                Ig = eo_POT[P, O, T][v][d1][d2][kx][ky][kz]
                Sg = eo_POT[P, O, T][v][s2*3+l1][s1*3+l2][kx][ky][kz]
                eo_SPOT[0][P, O, T][v][d1][d2][kx][ky][kz] = 0.5*(Ig + Sg)
                eo_SPOT[1][P, O, T][v][d1][d2][kx][ky][kz] = 0.5*(Ig - Sg)
        # print("Spin Parity Orb Time max: ", np.amax(np.linalg.norm(eo_SPOT)))

        if analysis:
            eo_SPO = np.sum(eo_SPOT, axis=3)
            eo_SPT = np.sum(eo_SPOT, axis=2)
            # eo_SOT = np.sum(eo_SPOT, axis=1)
            eo_POT = np.sum(eo_SPOT, axis=0)

            eo_SP = np.sum(eo_SPO, axis=2)
            eo_SO = np.sum(eo_SPO, axis=1)
            eo_ST = np.sum(eo_SPT, axis=1)
            # eo_PO = np.sum(eo_POT, axis=2)
            # eo_PT = np.sum(eo_POT, axis=1)
            eo_OT = np.sum(eo_POT, axis=0)

            eo_S = np.sum(eo_SP, axis=1)
            eo_P = np.sum(eo_SP, axis=0)
            eo_O = np.sum(eo_SO, axis=0)
            eo_T = np.sum(eo_ST, axis=0)

            print("Spin:")
            for S in range(2):
                text = "\t"
                if not S:
                    text += "even-spin"
                else:
                    text += "odd-spin"
                print(text, np.amax(np.linalg.norm(eo_S[S])))
            print("Parity:")
            for P in range(2):
                text = "\t"
                if not P:
                    text += "even-parity"
                else:
                    text += "odd-parity"
                print(text, np.amax(np.linalg.norm(eo_P[P])))
            print("Orbitals:")
            for O in range(2):
                text = "\t"
                if not O:
                    text += "even-orbital"
                else:
                    text += "odd-orbital"
                print(text, np.amax(np.linalg.norm(eo_O[O])))
            print("Frequencies:")
            for T in range(2):
                text = "\t"
                if not T:
                    text += "even-freq"
                else:
                    text += "odd-freq"
                print(text, np.amax(np.linalg.norm(eo_T[T])))

            print("\n")

            def label_SPOT(S, P, O, T):
                text = ""
                if not S:
                    text += "+S"
                else:
                    text += "-S"
                if not P:
                    text += "+P"
                else:
                    text += "-P"
                if not O:
                    text += "+O"
                else:
                    text += "-O"
                if not T:
                    text += "+T"
                else:
                    text += "-T"
                return text

            maxerror = 0
            for S, P, O, T in itp(range(2), repeat=4):
                norm = np.amax(np.linalg.norm(eo_SPOT[S][P][O][T]))
                if int((S+P+O+T) % 2) == 0:
                    if maxerror < norm:
                        maxerror = norm
                else:
                    print(label_SPOT(S, P, O, T), str(int(100*norm)))
            print("Max error on Pauli: ", maxerror)

        return eo_SPOT, eo_SPOT2

    def plotGap2(self, plt_obj, basis, params, savePath=None):
        """
        Give a 2D array of the gap function. Plot nicely all ndim*ndim gap
        element. Very useful with eo_SPOT objects.
        plt_obj : 2D np.array
                  Take eo_SPOT functions from self.SPOT_construct.
        basis : list of str
                Labels for each components of the basis.
        """
        ndim, m = self.ndim, self.mesh
        mx, my = m.n[0], m.n[1]
        # mz = m.n[2]

        default = PlotParams()
        default["subplots"] = [ndim, ndim]
        default["indices"] = [[[i, j] for i in range(ndim)]
                              for j in range(ndim)]
        default["figsize"] = (12, 12)
        default["title"] = ""
        default["titlesize"] = 8
        default["tight_layout"] = True
        default["show"] = True

        xlabels = [r"$\Re\Delta_{[&l1]},{[&l2]}$",
                   r"$\Im\Delta_{[&l1],[&l2]}$"]
        default["xlabels"] = [[xlabels for i in range(ndim)]
                              for j in range(ndim)]
        default["ylabels"] = None
        default["xticks"] = [None]
        default["yticks"] = [None]
        default["ticksize"] = 10
        xlim, ylim = [0, 2*mx-1], [0, 2*my-1]
        default["xlim"] = [[xlim for i in range(ndim)] for j in range(ndim)]
        default["ylim"] = [[ylim for i in range(ndim)] for j in range(ndim)]

        default["cmap"] = [["bwr" for i in range(ndim)] for j in range(ndim)]
        default["bar_format"] = [["%0.0e" for i in range(ndim)]
                                 for j in range(ndim)]
        default["bar_ticksize"] = 6

        default["sharey"] = False
        default["sharex"] = False
        default["special1"] = False
        default["special2"] = False
        default["legend_loc"] = None

        params.setDefaults(default)

        fig, ax = setFigure(params)
        for d1, d2 in itp(range(ndim), repeat=2):
            l1 = basis[d1]
            l2 = basis[d2]
            printVariables = {"l1": r"%s" % l1, "l2": r"%s" % l2}
            plotGrid2(ax, d1, d2, [plt_obj[d1][d2][:, :, 1].real,
                                   plt_obj[d1][d2][:, :, 1].imag],
                      0, m, params, printVariables, nophysobj=True)
        setSubplot(ax, params)
        finish(params, saveplace=savePath)

    def TestAllSym(self, basis="Orb", kset=None, precision=1e-10):
        r"""Crystal symmetry:
           From a gap objects that is read, with a definitive kptmesh, apply
           all the crystal symmetries such as
                r"\Delta(gk) = G * \Delta(k) * G^{-1}"
           where g a the symmetry matrix on k point defined by the space group
           and G is the corresponding transformation matrix in the choosen
           basis. Returns all broken symmetries.

           Odd-frequency:
           Check if even or odd under frequenecy by comparing
           "\Delta(\vec{k}, -w)_{l_2, l_1} ="
           " \pm \Delta(\vec{k}, w)_{l_1, l_2}."
           Broken-symmetry is written as 'w'.

        Parameters
        ----------
        basis : str
                Defines the space where symmetries are checked. For now, the
                main purpose is in the orbital basis.
        kset : it.product
               Set of k points to check. If None, check all.
        precision : float
                    Precision of the comparaison.
        """

        m = self.mesh

        if not m.nkpt:
            raise ValueError("Mesh not loaded.")
        # Symmetries enconded in the space group
        gs = m.sgroup.gs
        if basis == "Orb":
            # We need to tell the mesh what is the orbital space to act on
            # That is because the matrices in orbital space are non-trivial
            if not len(m.sgroup.orbs*self.nspin) == self.ndim:
                raise ValueError("Mesh has not the same orbital dimension as "
                                 "the gap functions.")
        else:
            raise NotImplementedError("The TestCrystalSym function is not "
                                      "implemented for this basis yet.")

        # Construct full gap object, in dims of (nkpt, nfreq, ndim, ndim)
        Gap = self.Re.Obj + 1j*self.Im.Obj

        # Stats information
        broken_sym = []
        err = 0
        nk = 0

        # Writing for debug
        np.set_printoptions(linewidth=10000, threshold=10000, precision=5)

        if kset is None:
            kset = itp(range(2*m.n[0]), range(2*m.n[1]), range(2*m.n[2]))

        def labelSpinToGens(text):
            S = np.diag([1, 1])
            for op in re.split(r"(\W)", text)[::-1]:
                if op == "":
                    break
                if op != "*":
                    S = S.dot(m.sgroup.spinGen[op])
            return S

        # Check each points
        # TODO: Make it more efficient?
        for nx, ny, nz in kset:

            # Gap is unidimensional in kpts, so keep track of index
            mpt1 = m[nx, ny, nz]
            # Only check at encoded points (just be all the FBZ)
            if mpt1 != -1:
                nk += 1
                err_k = 0
                # Symmetries are done around the center, hence k here
                n1 = np.transpose(np.matrix(m.npts[mpt1]))
                k1 = n1-m.N0
                Gap1 = Gap[mpt1]
                # Crystal symmetries
                # ------------------
                for g in gs:
                    # Associate symmetry matrix for orbital representation
                    T_orb = self.mesh.sgroup.GtoOrb(g)
                    Ti_orb = ln.inv(T_orb)
                    if self.nspin == 2:
                        T_label = m.sgroup.gO[g]
                        T_spin = labelSpinToGens(T_label)
                        Ti_spin = ln.inv(T_spin)
                        T_orbspin = np.kron(T_spin, T_orb)
                        Ti_orbspin = np.kron(Ti_spin, Ti_orb)
                    else:
                        T_orbspin = T_orb
                    # Symmetric point information
                    k2 = m.sgroup.GtoMatrix(g).dot(k1)
                    n2 = k2+m.N0
                    n2 = m.translate_in_FBZ([n2.item(0), n2.item(1),
                                             n2.item(2)])
                    n2x = int(round(n2[0]))
                    n2y = int(round(n2[1]))
                    n2z = int(round(n2[2]))
                    mpt2 = m[n2x, n2y, n2z]
                    Gap2 = Gap[mpt2]
                    # Check all frequencies
                    for w in range(self.nfreq):
                        T = T_orbspin
                        Tt = np.transpose(T)
                        G2 = Tt.dot(Gap2[w]).dot(T)
                        # Detects symmetry breaking
                        if (np.amax(np.absolute(Gap1[w] - G2)) > precision):
                            # print(np.amax(np.absolute(Gap1[w]-G2)))
                            sumg = np.amax(np.linalg.norm(Gap1[w]+G2))
                            # Add broken symmetry if new
                            if g not in broken_sym:
                                print()
                                print("---------")
                                broken_sym.append(g)
                                # Debugging prints
                                print("New Broken sym")
                                print(g)
                                print("k1, k2")
                                print(np.transpose(n1), n2)
                                print(m.sgroup.gO[g])
                                print("DIFF")
                                print(np.amax(np.linalg.norm(Gap1[w]-G2)))
                            elif sumg > precision:
                                print("---------")
                                print(g)
                                print(m.sgroup.gO[g])
                                print("DIFF")
                                print(np.amax(np.linalg.norm(Gap1[w]-G2)))
                            if sumg > precision:
                                print("SUM")
                                print(sumg)
                                print(np.amax(np.absolute(Gap1[w]+G2)))
                                np.set_printoptions(precision=3)
                                print("Gap(r1)")
                                print(Gap1[w])
                                print("Gap(r2)")
                                print(Gap2[w])
                                print("Ti.Gap(r2).T")
                                print(G2)
                                print("T_label")
                                print(T_label)
                                print("T_orb")
                                print(T_orb)
                                print("T_spin")
                                print(T_spin)
                                print(ln.inv(T_spin))
                                print("T_orbspin")
                                print(T_orbspin)
                                print("Ti_orbspin")
                                print(Ti_orbspin)
                                print("inv T_orbspin")
                                print(ln.inv(T_orbspin))

                            if np.amax(np.absolute(Gap1[w] + G2)) > precision:
                                err_k = 1
                if err_k:
                    err += 1

                odd_freq = None
                # Odd-frequency check
                # -------------------
                if self.nspin == 1:
                    for l1, l2 in itp(range(self.ndim), range(self.ndim)):
                        for w in range(round(self.nfreq/2)):
                            w2 = self.nfreq - 1 - w

                            gapV1 = Gap[mpt1, w, l1, l2]
                            gapV2 = Gap[mpt1, w2, l2, l1]
                            diff_gap = gapV1 - gapV2
                            sum_gap = gapV1 + gapV2

                            if np.amax(np.absolute(diff_gap)) < precision:
                                iseven = True
                            else:
                                iseven = False

                            if np.amax(np.absolute(sum_gap)) < precision:
                                isodd = True
                            else:
                                isodd = False

                            if iseven and isodd:
                                break
                            elif iseven:
                                if odd_freq is None:
                                    odd_freq = "even-w"
                                elif odd_freq == "odd-w":
                                    mess = ("Inconsistent odd-frequency "
                                            "symmetry! Check and document.")
                                    raise ValueError(mess)
                            elif isodd:
                                if odd_freq is None:
                                    odd_freq = "odd-w"
                                elif odd_freq == "even-w":
                                    mess = ("Inconsistent odd-frequency "
                                            "symmetry! Check and document.")
                                    raise ValueError(mess)

                            if not iseven and not isodd and self.nspin == 1:
                                raise ValueError("Perhaps degeneracy. "
                                                 "Document.")

                    if odd_freq is not None:
                        if odd_freq in broken_sym:
                            pass
                        elif ("even-w" not in broken_sym and "odd-w" not in
                              broken_sym):
                            broken_sym.append(odd_freq)
                        else:
                            raise ValueError("Inconsistent odd-frequency for "
                                             "different k points.")

        # Print stats
        print("Number of k points with mesh != -1: %d" % nk)
        print("Number of k pts with error: %d" % err)
        print(broken_sym)
        # Return broken syms
        return broken_sym

    def interpolate(self, mesh2):
        """From a gap function defined on a given mesh, interpolate to a finer
           mesh. Returns another object of type Gap.

        Parameters
        ----------
        mesh2 : KptMesh
                Mesh for the interpolated gap object.
        """
        m = self.mesh
        mx, my, mz = m.n[0], m.n[1], m.n[2]
        m2 = mesh2
        m2x, m2y, m2z = m2.n[0], m2.n[1], m2.n[2]

        mgap = np.zeros((2*mx+1, 2*my+1, 2*mz+1, self.nfreq,
                         self.ndim, self.ndim), dtype=complex)

        q_set = itp(range(2*mx+1), range(2*my+1), range(2*mz+1))
        for qx, qy, qz in q_set:
            q = m.translate_in_FBZ([qx-1, qy-1, qz-1])
            mgap[qx, qy, qz] = self[m[q[0], q[1], q[2]], :]

        interpol = np.zeros((self.nfreq, self.ndim, self.ndim),
                            dtype=GridInt)
        for f in range(self.nfreq):
            for l1, l2 in itp(range(self.ndim), range(self.ndim)):
                linx = np.linspace(-1, 2*mx-1, 2*mx+1)
                liny = np.linspace(-1, 2*my-1, 2*my+1)
                linz = np.linspace(-1, 2*mz-1, 2*mz+1)
                interpol[f, l1, l2] = GridInt((linx, liny, linz),
                                              mgap[:, :, :, f, l1, l2])

        q_set2 = itp(range(2*m2x), range(2*m2y),
                     range(2*m2z))
        gap2 = np.zeros((m2x*m2y*m2z*2, self.nfreq,
                         self.ndim, self.ndim), dtype=complex)

        numq = 8*m2x*m2y*m2z
        numdone = 0
        prct = 0

        i = 0
        for qx, qy, qz in q_set2:
            if numdone / numq * 100 >= prct:
                print("[%d/100]" % prct)
                prct += 1
            numdone += 1
            q2x = 1.*(qx-m2.N0.item(0))*mx/m2x+m.N0.item(0)
            q2y = 1.*(qy-m2.N0.item(1))*my/m2y+m.N0.item(1)
            q2z = 1.*(qz-m2.N0.item(2))*mz/m2z+m.N0.item(2)
            if m2.in_FBZ(m2.fromNgetM([qx, qy, qz])):
                m2[qx, qy, qz] = i
                m2.npts.append([qx, qy, qz])
                for l1, l2 in itp(range(self.ndim), range(self.ndim)):
                    for f in range(self.nfreq):
                        v = interpol[f, l1, l2](np.array([q2x, q2y, q2z]))
                        gap2[i, f, l1, l2] = v
                i += 1
        m2.nkpt = len(mesh2.npts)

        Gap2 = Gap(self.ndim, mesh2, self.nfreq, nspin=self.nspin)
        Gap2.Re.Obj = gap2.real
        Gap2.Im.Obj = gap2.imag
        return Gap2

    def changeBasis(self, proj, pmesh):
        """The gap function is obtained in the orbital basis, but to calculate
           physical quantities from it, we need it first in the band basis. By
           giving a set of projectors, probably defined on a coarser grid, this
           this function calculates and returns the gap in the band basis.

        Parameters
        ----------
        proj : Projectors
               Set of projectors allowing the change between orbital and band
               basis.
        pmesh : KptMesh
                Mesh on which the projectors are defined. Because these are
                calculated for the BareChi, it is probably a much coarser mesh
                than the gap, which needs to be relatively small for the
                Eliashberg solving. This mesh needs to be a submesh of the gap
                mesh, but it has to be anyway.
        """
        # Proj can only work for one atom now, easy to do.
        if proj.natom > 1:
            raise ValueError("Only works for one correlated atom now.")
        if proj.nspin > 2:
            raise ValueError("Only works for one or two spin now.")

        # Full gap object
        gap = self.Re.Obj + 1j*self.Im.Obj
        # Gap in band basis
        gap_band = Gap(proj.nband, self.mesh, self.nfreq)
#        gap_band = np.zeros((self.mesh.nkpt, self.nfreq, proj.nband,
#                             proj.nband), dtype=complex)
        m = self.mesh
        numk = 8*m.n[0]*m.n[1]*m.n[2]
        numdone = 0
        prct = 0

        # Function for returning the right spin labels
        def labelSpinToGens(text):
            S = np.diag([1, 1])
            for op in re.split(r"(\W)", text):
                if op == "":
                    break
                if op != "*":
                    S = S.dot(m.sgroup.spinGen[op])
            return S

        mproj = []
        for a in range(proj.natom):
            mproj.append(np.zeros((2*pmesh.n[0], 2*pmesh.n[1], 2*pmesh.n[2],
                                   proj.nspin*proj.norb[a], proj.nband),
                                  dtype=complex))
            for nx, ny, nz in itp(range(2*pmesh.n[0]), range(2*pmesh.n[1]),
                                  range(2*pmesh.n[2])):
                kpt = pmesh.mesh[nx, ny, nz]
                if kpt != -1:
                    for at in range(proj.natom):
                        for sp in range(proj.nspin):
                            sp1 = sp*proj.norb[at]
                            sp2 = (sp+1)*proj.norb[at]
                            tempproj = proj.projectors[at][kpt][sp]
                            mproj[at][nx, ny, nz, sp1:sp2] = tempproj

        # Calculating on each mesh point
        for nx, ny, nz in itp(range(2*m.n[0]),
                              range(2*m.n[1]),
                              range(2*m.n[2])):
            if numdone / numk * 100 >= prct:
                print("[%d/100]" % prct)
                prct += 1
            numdone += 1
            # K point mesh index for the gap, should be a full mesh already
            i1_gap = m[nx, ny, nz]
            if i1_gap != -1:
                # Obtaining the k point mesh index on the projectors' mesh
                # This is a bit tricky but using the value in reciprocal basis,
                # it becomes easy
                m1 = self.mesh.fromNgetM([nx, ny, nz])
                n1_proj = pmesh.fromMgetN([m1.item(0), m1.item(1),
                                           m1.item(2)])
                print(nx, ny, nz)
                print(m1)
                print(n1_proj)
                n1x = round(n1_proj.item(0))
                n1y = round(n1_proj.item(1))
                n1z = round(n1_proj.item(2))
                # K point mesh index on the projectors' mesh
                i1_proj = pmesh[n1x, n1y, n1z]

                # Perhaps it is not defined since projectors mesh probably is
                # reduced. If defined, easy, otherwise calculated
                if i1_proj != -1:
                    # pp = proj[0][i1_proj, 0]
                    pp = mproj[0][n1x, n1y, n1z]
                else:
                    pp = False
                    # Looking for a defined symmetric point
                    for sym in pmesh.sgroup.gs:
                        g = pmesh.sgroup.GtoMatrix(sym)
                        G = pmesh.sgroup.GtoOrb(sym)
                        # G = ln.inv(G)
                        if proj.nspin == 2:
                            S = pmesh.sgroup.gO[sym]
                            S = labelSpinToGens(S)
                            # S = np.conj(S)
                            S = np.transpose(S)
                            G = np.kron(S, G)
                            # G = ln.inv(G)
                            # G = np.conj(G)
                        n2 = g.dot(n1_proj-pmesh.N0)+pmesh.N0
                        n2 = pmesh.translate_in_FBZ([n2.item(0),
                                                     n2.item(1),
                                                     n2.item(2)])
                        n2x = round(n2[0])
                        n2y = round(n2[1])
                        n2z = round(n2[2])
                        i2 = pmesh[n2x, n2y, n2z]
                        if i2 != -1:
                            # Projectors' value
                            # pp = G.dot(proj[0][i2, 0])
                            pp = G.dot(mproj[0][n2x, n2y, n2z])

                            # pp = np.zeros(pp.shape)
                            ("n: ", n1x, n1y, n1z, n2x, n2y, n2z)
                            print("sym")
                            print(sym)
                            print("g")
                            print(g)
                            print("G")
                            print(G)
                            print("P")
                            print(mproj[0][n2x, n2y, n2z])
                            print("pp = G P")
                            print(pp)
                            break
                    # No symmetric point found = error
                    if pp is False:
                        text = ("This n point has empty projectors and is "
                                "not link by symmetry to any other point "
                                "with none-empty projectors.")
                        raise ValueError(text)
                # pp_d = np.transpose(np.conj(pp))
                pp_t = np.transpose(pp)
                # pp_c = np.conj(pp)

                # print("n: ", n1x, n1y, n1z)
                # print("pp:")
                # print(pp)
                # print("pp_t:")
                # print(pp_t)
                # Calculate the basis change for all frequencies
                for w in range(self.nfreq):
                    # gap_band[i1_gap, w] = pp_t.dot(gap[i1_gap, w]).dot(pp_c)
                    gap_band[i1_gap, w] = pp_t.dot(gap[i1_gap, w]).dot(pp)
                    # gap_band[i1_gap, w] = pp_d.dot(gap[i1_gap, w]).dot(pp_c)
                    # gap_band[i1_gap, w] = pp_d.dot(gap[i1_gap, w]).dot(pp)
                    # print("w:", w)
                    # print("gap:")
                    # print(gap[i1_gap, w])
                    # print("gap_band:")
                    # print(gap_band[i1_gap, w])

        # Return the gap function in the band basis
        return gap_band

    def IrrepProjection(self, irrep_num, basis="SpinOrb"):
        if not self.mesh.nkpt:
            raise ValueError("Mesh not loaded.")

        sgroup = self.mesh.sgroup
        opers = sgroup.irreps_op
        if basis == "Orb":
            if not len(self.mesh.sgroup.orbs)*self.nspin == self.ndim:
                raise ValueError("Mesh has not the same orbital dimension as "
                                 "the gap function.")
        elif basis == "Spin":
            print("Write comment here.")
        elif basis == "SpinOrb":
            if not len(self.mesh.sgroup.orbs)*self.nspin == self.ndim:
                raise ValueError("Mesh has not the same orbital dimension as "
                                 "the gap function.")
            print("Write comment here.")
        else:
            raise NotImplementedError("The returnIrrep function is not "
                                      "implemented for this basis yet.")

        gap = Gap(self.ndim, self.mesh, self.nfreq, nspin=self.nspin)

        Gaps = [self.Re.Obj + 1j*self.Im.Obj]

        kset = itp(range(2*self.mesh.n[0]), range(2*self.mesh.n[1]),
                   range(2*self.mesh.n[2]))

        print(sgroup.irreps)
        # characters = [0*Gaps[0] for i in sgroup.irreps]

        def labelSpinToGens(label):
            T_spin = np.diag([1, 1])
            for op in re.split(r"(\W)", label)[::-1]:
                if op == "":
                    break
                if op != "*":
                    T_spin = T_spin.dot(sgroup.spinGen[op])
            return T_spin

        for nx, ny, nz in kset:
            mpt1 = self.mesh[nx, ny, nz]
            if mpt1 != -1:
                n1 = np.transpose(np.matrix(self.mesh.npts[mpt1]))
                k1 = n1 - self.mesh.N0
                # Gap1 = Gaps[0][mpt1]

                for o, op in enumerate(opers):
                    for g in op:
                        if basis in ["SpinOrb", "Orb"]:
                            T_orb = sgroup.GtoOrb(g)
                        elif basis in ["Spin"]:
                            T_orb = sgroup.GtoOrb("I+I+I")
                        else:
                            raise ValueError("Wrong basis argument.")

                        if self.nspin == 2:
                            if basis in ["SpinOrb", "Spin"]:
                                T_label = sgroup.gO[g]
                            elif basis in ["Orb"]:
                                T_label = sgroup.gO["I+I+I"]
                            T_spin = labelSpinToGens(T_label)
                            T = np.kron(T_spin, T_orb)
                        else:
                            T = T_orb
                        k2 = sgroup.GtoMatrix(g).dot(k1)
                        n2 = k2+self.mesh.N0
                        n2 = self.mesh.translate_in_FBZ([n2.item(0),
                                                         n2.item(1),
                                                         n2.item(2)])
                        n2x = int(round(n2[0]))
                        n2y = int(round(n2[1]))
                        n2z = int(round(n2[2]))
                        mpt2 = self.mesh[n2x, n2y, n2z]
                        Gap2 = Gaps[0][mpt2]

                        Tt = np.transpose(T)
                        for w in range(self.nfreq):
                            chip = sgroup.irreps_table[irrep_num][o]
                            G2 = Tt.dot(Gap2[w]).dot(T)/len(sgroup.gO)
                            gap.Re.Obj[mpt1, w] += (chip*G2).real
                            gap.Im.Obj[mpt1, w] += (chip*G2).imag

        return gap

    def returnIrrep(self, degGaps=None, basis="Orb", kset=None,
                    precision=1e-10):
        """The gap function has a certaine symmetry"""
        if not self.mesh.nkpt:
            raise ValueError("Mesh not loaded.")

        sgroup = self.mesh.sgroup
        ops = sgroup.irreps_op
        if basis == "Orb":
            if not len(self.mesh.sgroup.orbs)*self.nspin == self.ndim:
                raise ValueError("Mesh has not the same orbital dimension as "
                                 "the gap function.")
        elif basis == "Spin":
            print("Write comment here.")
        elif basis == "SpinOrb":
            if not len(self.mesh.sgroup.orbs)*self.nspin == self.ndim:
                print(self.mesh.sgroup.orbs)
                print(self.nspin)
                print(self.ndim)
                raise ValueError("Mesh has not the same orbital dimension as "
                                 "the gap function.")
            print("Write comment here.")
        else:
            raise NotImplementedError("The returnIrrep function is not "
                                      "implemented for this basis yet.")

        Gaps = [self.Re.Obj + 1j*self.Im.Obj]

        if kset is None:
            kset = itp(range(2*self.mesh.n[0]), range(2*self.mesh.n[1]),
                       range(2*self.mesh.n[2]))

        if degGaps is not None:
            for deg in degGaps:
                Gaps.append(deg)

        print(sgroup.irreps)
        characters = [0*Gaps[0] for i in sgroup.irreps]

        def labelSpinToGens(label):
            T_spin = np.diag([1, 1])
            for op in re.split(r"(\W)", label)[::-1]:
                if op == "":
                    break
                if op != "*":
                    T_spin = T_spin.dot(sgroup.spinGen[op])
            return T_spin

        for nx, ny, nz in kset:
            mpt1 = self.mesh[nx, ny, nz]
            k_characters = [0*Gaps[0][0] for i in sgroup.irreps]

            if mpt1 != -1:
                n1 = np.transpose(np.matrix(self.mesh.npts[mpt1]))
                k1 = n1 - self.mesh.N0
                Gap1 = Gaps[0][mpt1]
                if np.amax(np.absolute(Gap1)) < precision:
                    continue

                for o, op in enumerate(ops):
                    for g in op:
                        if basis in ["SpinOrb", "Orb"]:
                            T_orb = sgroup.GtoOrb(g)
                        elif basis in ["Spin"]:
                            T_orb = sgroup.GtoOrb("I+I+I")
                        else:
                            raise ValueError("Wrong basis argument.")

                        if self.nspin == 2:
                            if basis in ["SpinOrb", "Spin"]:
                                T_label = sgroup.gO[g]
                            elif basis in ["Orb"]:
                                T_label = sgroup.gO["I+I+I"]
                            T_spin = labelSpinToGens(T_label)
                            T = np.kron(T_spin, T_orb)
                        else:
                            T = T_orb
                        k2 = sgroup.GtoMatrix(g).dot(k1)
                        n2 = k2+self.mesh.N0
                        n2 = self.mesh.translate_in_FBZ([n2.item(0),
                                                         n2.item(1),
                                                         n2.item(2)])
                        n2x = int(round(n2[0]))
                        n2y = int(round(n2[1]))
                        n2z = int(round(n2[2]))
                        mpt2 = self.mesh[n2x, n2y, n2z]
                        Gap2 = Gaps[0][mpt2]
                        G2 = 0*Gap2

                        Tt = np.transpose(T)
                        # print("Tt")
                        # print(Tt)
                        # print("T")
                        # print(T)
                        # print("T_orb")
                        # print(T_orb)
                        # print("T_spin")
                        # print(T_spin)
                        # print("Gap2[0]")
                        # print(Gap2[0])
                        for w in range(self.nfreq):
                            G2[w] = Tt.dot(Gap2[w]).dot(T)

                        if np.amax(np.absolute(G2)) < precision:
                            continue
                        for i, irrep in enumerate(sgroup.irreps_table):
                            k_characters[i] += irrep[o]*G2

                    # if (np.amax(np.absolute(Gap1[w] - G2)) < precision):
                    #     k_characters.append(1)
                    # elif (np.amax(np.absolute(Gap1[w] + G2)) < precision):
                    #     k_characters.append(-1)
                    # else:
                    #     if degGap is None:
                    #         raise ValueError("This gap seems degenerate. You"
                    #                         " give the other degenerate gaps"
                    #                         " to find the irrep.")
                # for i, irreps in enumerate(sgroup.irreps):
                for i in range(len(sgroup.irreps)):
                    characters[i][mpt1] = k_characters[i]
                # null_characters = False
                # if not (characters == k_characters).all():
                #     print(n1)
                #     print("Characters: ", characters)
                #     print("K-Characters: ", k_characters)
                #     raise ValueError("Character ill-defined: changes for "
                #                      "different k-points.")

        solutions = []
        for i in range(len(sgroup.irreps)):
            valuemax = np.amax(np.absolute(characters[i]))
            diffmax = np.amax(np.absolute(characters[i] -
                              len(sgroup.gs)*Gaps[0]))
            if valuemax > precision:
                print(i, valuemax, diffmax)
                if not (diffmax < precision):
                    print("WARNING: It is not clear that this is the irrep. "
                          "Careful with degenerate solutions.")
                solutions.append(sgroup.irreps[i])
        # print("Characters[0]:")
        # print(characters[0])
        # print("16*Gap:")
        # print(16*Gap)
        return solutions
        raise ValueError("No irrep found.")

    def phaseDistribution(self, ntheta=200, prec=1e-5):
        ndim, nfreq = self.ndim, self.nfreq
        m = self.mesh
        nkpt = m.nkpt

        gapC = self.Re.Obj + 1j*self.Im.Obj

        distr = np.zeros((ndim, ndim, nfreq, 200))
        for k, w in itp(range(nkpt), range(nfreq)):
            for sl1, sl2 in itp(range(ndim), repeat=2):
                gC = gapC[k, w, sl1, sl2]
                phase = cmath.phase(gC) + math.pi
                norm = math.sqrt(gC*np.conj(gC))
                theta = math.floor(phase / 2 / math.pi * ntheta)
                if theta == 200:
                    theta = 0
                # print(theta, phase)
                if norm > prec:
                    distr[sl1, sl2, w, theta] += 1

        return distr
    # def time_rev(self):
    #     ndim, nspin, freq, m = self.ndim, self.nspin, self.nfreq, self.mesh
    #     norb, nkpt = int(ndim/nspin), m.nkpt

    #     orbs_lab = m.SGroup.Orbs
    #     spin_lab = [r"\uparrow", r"\downarrow"]
    #     basis = ["%s%s" % (orbs_lab[int(i%norb)], spin_lab[int(i/norb)])
    #              for i in range(ndim)]

    #     gapC = self.Re.Obj + 1j*gap.Im.Obj

    #     # Time-reversed gap
    #     TgapC = np.zeros((nkpt, nfreq, ndim, ndim), dtype=complex)

    #     global_phase = 0
    #     for k, w in itp(range(nkpt), range(nfreq)):
    #         n1 = np.traspose(np.matrix(m.npts[k])
    #         nn1 = n1 - m.N0
    #         nn2 = -1*nn1
    #         n2 = nn2 + m.N0
    #         n2 = m.translate_in_FBZ([n2.item(0), n2.item(1), n2.item(2)])
    #         n2x, n2y, n2z = (int(round(n2[0])), int(round(n2[1])),
    #                          int(round(n3[1])))
    #         k2 = m[n2x, n2y, n2z]

    #         w2 = -w-1+nfreq

    def __getitem__(self, key):
        return self.Re.Obj[key] + 1j*self.Im.Obj[key]

    def __setitem__(self, key, value):
        self.Re.Obj[key] = value.real
        self.Im.Obj[key] = value.imag
