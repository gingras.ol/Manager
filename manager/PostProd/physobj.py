from .kptmesh import KptMesh
import pylab as pl
import os
pl.set_printoptions(threshold=1000, linewidth=132, precision=3)


# This object is a physical objects, can be plotted, rotate, etc
class PhysObj:
    def __init__(self, nnorb, nkpt, nfreq=1, linear=False, kptprob=False):
        self.init = 0
        self.linear = linear

        # The object has two orbital two indexes (l1l2, l3l4),
        # a frequency index (v) and a kpt index (k)
        # self.Obj = [[pl.zeros((nnorb, nnorb)) for v in range(nfreq)]
        #             for k in range(nkpt)]
        self.Obj = pl.zeros((nkpt, nfreq, nnorb, nnorb))
        self.Trace = [[pl.zeros((nnorb + 1)) for v in range(nfreq)]
                      for k in range(nkpt)]

        self.shape = ("[ (" + str(nnorb) + ", " + str(nnorb) + "), " +
                      str(nfreq) + ", " + str(nkpt) + "]")
        self.n = [(nnorb, nnorb), nfreq, nkpt]
        self.nnorb = nnorb
        self.nfreq = nfreq
        self.nkpt = nkpt

        print("PhysObj.__init__: Object well initiated and has dimensions " +
              self.shape + ".")
        self.init = 1
        self.kptprob = kptprob

    def isinit(self):
        return self.init

    def Read(self, _file, _skip=0, _skipc=0):
        print("PhysObj.Read: Filling the object from file "+_file+'.')

        if not os.path.exists(_file):
            raise ValueError("This file does not exist.")

        with open(_file, 'r') as f:
            for i, line in enumerate(f):
                if i > _skip - 1:
                    if not self.linear:
                        if ((i - _skip) % (self.nnorb + 1)) == 0:
                            if len(line.split()) == 1:
                                if self.kptprob:
                                    w = int(line.split()[0][0])
                                    k = int(line.split()[0][1:]) - 1
                                else:
                                    w = 0
                                    k = int(line.split()[0]) - 1
                            elif len(line.split()) == 2:
                                w = int(line.split()[0])
                                k = int(line.split()[1]) - 1
                        else:
                            l1 = ((i - _skip) % (self.nnorb + 1) - 1)
                            for l2 in range(self.nnorb):
                                f = float(line.split()[l2])
                                self.Obj[k][w][l1][l2] = f
                                if l1 == l2:
                                    self.Trace[k][w][l1] = f
                                    self.Trace[k][w][-1] += f
                    else:
                        for j in range(len(line.split())):
                            k = int((i - _skip) / self.nfreq)
                            w = int((i - _skip) % self.nfreq)
                            if j != 0:
                                l1 = int((j - 1 - _skipc) / self.nnorb)
                                l2 = (j - 1 - _skipc) % self.nnorb
                                # print("k, w, l1, l2, elem: ",
                                #       k, w, l1, l2, line.split()[j])
                                f = float(line.split()[j])
                                self.Obj[k][w][l1][l2] = f
                                if l1 == l2:
                                    self.Trace[k][w][l1] = f
                                    self.Trace[k][w][-1] += f
        print('PhysObj.Read: Done filling the object.')

    def write(self, filename, topline=None):
        """Write PhysObj.
        """
        with open(filename, "w") as f:
            if topline is not None:
                f.write(topline)

            print(self.nkpt)
            print(self.nfreq)
            print(self.nnorb)

            for freq in range(self.nfreq):
                for k in range(self.nkpt):
                    if self.nfreq > 1:
                        f.write("\n\t%d\t%d" % (freq, k+1))
                    else:
                        f.write("\n\t%d" % (k+1))
                    for l1 in range(self.nnorb):
                        line = "\n"
                        for l2 in range(self.nnorb):
                            line += "\t\t%.10f" % self[k, freq, l1, l2]
                        f.write(line)

    def ReturnInPlane(self, _mesh, _elems):
        if not isinstance(_mesh, KptMesh):
            print("ERROR in PhysObj.ReturnInPlane: You must give a mesh"
                  " which is a KptMesh object.")
        else:
            if self.n[-1] != _mesh.nkpt:
                print("ERROR in PhysObj.ReturnInPlane: Mesh does not have"
                      " the same number of kpts as the object.")
            else:
                if len(_elems) != len(self.n)-1:
                    print("ERROR in PhysObj.ReturnInPlane: You need to give"
                          " the elements to print in an array, first the"
                          " tupple for the matrix element, then the other"
                          " dof excluding the kpts.")
                else:
                    ww = _elems[1]
                    ll = _elems[0]

                    print("PhysObj.ReturnInPlane: Component"
                          " asked is element " + str(ll) + " with w " +
                          str(ww) + ".")
                    NewObj = pl.zeros((2*_mesh.n[0], 2*_mesh.n[1]))
                    for k in range(_mesh.nkpt):
                        kpt = _mesh.npts[k]
                        if kpt[2] == _mesh.n[2] - 1:
                            if len(ll) == 2:
                                o = self.Obj[k][ww][ll[0]][ll[1]]
                                NewObj[kpt[0], kpt[1]] = o
                            elif ll == self.n[0][0]:
                                NewObj[round(kpt[0]),
                                       round(kpt[1])] = self.Trace[k][ww][-1]
                            elif ll in range(self.n[0][0]):
                                NewObj[round(kpt[0]),
                                       round(kpt[1])] = self.Trace[k][ww][ll]
                            else:
                                print("ERROR in PhysObj.ReturnInPlane: _elems"
                                      " selected do not match any options.")
                    print('PhysObj.ReturnInPlane: Returning object.')
                    return pl.around(pl.transpose(NewObj), decimals=9)

    def __getitem__(self, key):
        return self.Obj[key]

    def __setitem__(self, key, value):
        self.Obj[key] = value

    def __len__(self):
        return len(self.Obj)

    def __add__(self, b):
        if not isinstance(b, PhysObj):
            raise TypeError("Cannot add something else than a PhysObj to"
                            " a PhysObj.")
        new = PhysObj(self.nnorb, self.nkpt, self.nfreq)
        new.Obj = self.Obj + b.Obj
        return new
