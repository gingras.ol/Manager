from manager.Calculations import Calculation, Dressed
from colorama import Fore, Back, Style
from manager.PostProd import PhysObj, KptMesh, plotting, PlotParams, Gap
import mpl_toolkits.axes_grid1.inset_locator as inset_axes
import itertools as it
import pylab as pl
import colorama
import math
import re
import os
# import sys


class Eliash(Calculation):

    def __init__(self, system, conn, id=-1, nspin=1, ndim=1, pseudo=0):
        Calculation.__init__(self, system, conn)
        self.table = "eliash"
        self.prevTable = ["dressed", "barechi2"]
#        self.prevTable = ["dressed", "barechi2", "barechi1", "dmft"]
        self.prevCalc = Dressed(system, conn)
        self.nspin = nspin

        self.params = {"parity": None, "liwn": None, "livn": None,
                       "liwn_max": None, "nvec": None}

        filePath = re.match(r"^(.+/)\w+\.\w+$",
                            os.path.realpath(__file__)).group(1)

        with open("%s/Eliash/Input.dat" % filePath, "r") as f:
            fileInput = f.read()
        fileInput = re.sub(r"\*\(system\)", system, fileInput)

        def funcSOC():
            if nspin == 1:
                return "0"
            elif nspin == 2 and pseudo == 0:
                return "1"
            elif nspin == 2 and pseudo == 2:
                return "2"
        self.funcParams["SOC"] = funcSOC

        def funcDim():
            return str(ndim)
        self.funcParams["norbnspin"] = funcDim

        if nspin == 1:
            def funcParity():
                parity = self.params["parity"]
                if parity == 0 or parity == "0":
                    return "Singlet"
                elif parity == 1 or parity == "1":
                    return "Triplet"
                else:
                    print("Error: parity neither 0 or 1, is %s" % parity)
            self.funcParams = {"parity": funcParity}

        if nspin == 1:
            self.treeDir = ("U-&(dressed.dressedU)/J-&(dressed.dressedJ)/"
                            "!(parity)/")
        elif nspin == 2:
            self.treeDir = ("U-&(dressed.dressedU)_J-&(dressed.dressedJ)_"
                            "Up-&(dressed.dressedUp)_"
                            "Upp-&(dressed.dressedUpp)_"
                            "Jp-&(dressed.dressedJp)/")
        d = "&(dressed.dresseddir)"
        b = "&(barechi2.barechi2dir)"
        if nspin == 1:
            ind = {"Input.dat": fileInput,
                   ("[%s.BareIntphDensity]" %
                    system): (d + "Input/%s.BareIntphDensity" % system),
                   ("[%s.BareIntphMagnetic]" %
                    system): (d + "Input/%s.BareIntphMagnetic" % system),
                   ("[%s.ImDressedSusphDensity]" %
                    system): (d + "Output/%s.ImDressedSusphDensity" % system),
                   ("[%s.ReDressedSusphDensity]" %
                    system): (d + "Output/%s.ReDressedSusphDensity" % system),
                   ("[%s.ImDressedSusphMagnetic]" %
                    system): (d + "Output/%s.ImDressedSusphMagnetic" % system),
                   ("[%s.ReDressedSusphMagnetic]" %
                    system): (d + "Output/%s.ReDressedSusphMagnetic" % system),
                   ("[%s.ImBareChipp]" %
                    system): (b + "Output/%s.ImBareChipp" % system),
                   ("[%s.ReBareChipp]" %
                    system): (b + "Output/%s.ReBareChipp" % system),
                   ("[%s.ImBareSuspp]" %
                    system): (b + "Output/%s.ImBareSuspp" % system),
                   ("[%s.ReBareSuspp]" %
                    system): (b + "Output/%s.ReBareSuspp" % system),
                   ("[%s.klist]" % system): (b + "Input/%s.qlist" % system)
                   }
        elif nspin == 2 and pseudo == 0:
            ind = {"Input.dat": fileInput,
                   ("[%s.BareIntph]" %
                    system): (d + "Input/%s.BareIntph" % system),
                   ("[%s.ImDressedSusph]" %
                    system): (d + "Output/%s.ImDressedSusph" % system),
                   ("[%s.ReDressedSusph]" %
                    system): (d + "Output/%s.ReDressedSusph" % system),
                   ("[%s.ImBareChipp]" %
                    system): (b + "Output/%s.ImBareChipp" % system),
                   ("[%s.ReBareChipp]" %
                    system): (b + "Output/%s.ReBareChipp" % system),
                   ("[%s.ImBareSuspp]" %
                    system): (b + "Output/%s.ImBareSuspp" % system),
                   ("[%s.ReBareSuspp]" %
                    system): (b + "Output/%s.ReBareSuspp" % system),
                   ("[%s.klist]" % system): (b + "Input/%s.qlist" % system)
                   }
        elif nspin == 2 and pseudo == 2:
            ind = {"Input.dat": fileInput,
                   ("[%s.ImGammapp1]" %
                    system): (d + "PrepPseudo/%s.ImGammapp1" % system),
                   ("[%s.ReGammapp1]" %
                    system): (d + "PrepPseudo/%s.ReGammapp1" % system),
                   ("[%s.ImGammapp2]" %
                    system): (d + "PrepPseudo/%s.ImGammapp2" % system),
                   ("[%s.ReGammapp2]" %
                    system): (d + "PrepPseudo/%s.ReGammapp2" % system),
                   ("[%s.ImBareChipp]" %
                    system): (d + "PrepPseudo/%s.ImBareChipp_pm" % system),
                   ("[%s.ReBareChipp]" %
                    system): (d + "PrepPseudo/%s.ReBareChipp_pm" % system),
                   ("[%s.klist]" % system): (b + "Input/%s.qlist" % system)
                   }
        self.tree = {"Output/": {},
                     "Input/": ind
                     }
        if id != -1:
            self.id = id
            if not self.Load(all=True):
                print("%sError: Could not load the calculations.%s" %
                      (Fore.BLACK + Back.RED, Style.RESET_ALL))
                return False

    def CreateTable(self):
        self.testConn()

        query = ("CREATE TABLE eliash ( "
                 "eliashid INTEGER NOT NULL PRIMARY KEY, "
                 "dressedid INTEGER, "
                 "eliashdir CHAR(255) NOT NULL, "
                 "parity INTEGER, "
                 "liwn INTEGER, "
                 "livn INTEGER, "
                 "liwn_max INTEGER, "
                 "nvec INTEGER, "
                 "eigs CHAR(255), "
                 "syms CHAR(255), "
                 "irreps CHAR(255), "
                 "status CHAR(255))")

        self.conn.Query("execute", query)

    def LoadGap(self, mesh, ngap="01"):
        if self.id == -1:
            raise ValueError("%sError: You cannot find leading eigenvalues"
                             " from this calculation since it has no defined "
                             "id%s" % (Back.RED + Fore.BLACK, Style.RESET_ALL))
        self.Load(all=True)
        nspin, norb = self.cells["barechi2.nspin"], self.cells["barechi2.norb"]
        ndim = nspin * norb
        nfreq = 2 * (1 + self.cells["eliash.liwn_max"])
        gap = Gap(ndim, mesh, nfreq, nspin=nspin)
        gap.Re.Read("%s%s/Output/%s.ReGapFn%s" %
                    (self.conn.groundDir, self.cells["eliash.eliashdir"],
                     self.system, ngap), _skip=1, _skipc=1)
        gap.Im.Read("%s%s/Output/%s.ImGapFn%s" %
                    (self.conn.groundDir, self.cells["eliash.eliashdir"],
                     self.system, ngap), _skip=1, _skipc=1)
        return gap

    def LoadVertex_ph1(self, nkpt):
        if self.id == -1:
            raise ValueError("Error: No id.")

        norb = self.cells["barechi2.norb"]
        nfreqs = self.cells["eliash.livn"] + 1

        Vertex_ph1 = PhysObj(norb**2, nkpt, nfreqs, linear=False, kptprob=True)
        Vertex_ph1.Read("%s%sOutput/%s.%s" % (self.conn.groundDir,
                        self.baseDir, self.system, "ReVertexLadderph_1"),
                        _skip=2)

        return Vertex_ph1

    def SetEigs(self, outputFile="output.txt"):
        """Automatically go fetch the resulting eigenvalues from
        Eliashberg calculation and puts them in the database.

        -----------
        Parameters:
        """
        colorama.init()

        self.id_check()
        self.Load(all=True)

        nvec = self.cells["eliash.nvec"]

        # Check that the output file is found
        # If not, the calculation's status is marked as 'error'
        # This should be made such that the outfile is fixed. TODO
        outputFile = (self.conn.groundDir + self.cells["eliash.eliashdir"] +
                      outputFile)
        if not os.path.exists(outputFile):
            print("%sError: File does not exists %s%s%s" %
                  (Fore.BLACK + Back.RED, Style.BRIGHT, outputFile,
                   Style.RESET_ALL))
            print("%sTip: Perhaps the outputFile is not recognized. Make sure"
                  "is has the right name.%s%s%s" %
                  (Fore.BLACK + Back.RED, Style.BRIGHT, outputFile,
                   Style.RESET_ALL))

            query = ("UPDATE eliash SET status = 'error' WHERE eliashid = %d"
                     % self.id)
            self.conn.Query("execute", query)
            return False

        # norb = self.cells["barechi2.norb"]

        leadEigs = ""
        with open(outputFile, "r") as f:
            v = 1
            for line in f:
                match = re.search(r"Row +(\d): +(\d\.\d+D[-\+]\d{2})", line)
                if match:
                    if v == 1:
                        leadEigs += re.sub("D", "e", match.group(2))
                    else:
                        leadEigs += "; " + re.sub("D", "e", match.group(2))
                    v = v + 1

        # If no leadEigs found, the calculation is set to incomplete
        # If they were found, the calculation is done and the leadEigs
        # are set in the database.
        if leadEigs == "":
            print("%sThis calculation was not finished.%s" %
                  (Fore.RED, Fore.RESET))
            self.conn.Query("execute",
                            "UPDATE eliash SET status = 'incomplete' WHERE"
                            " eliashid = %d" % self.id)
        elif v != nvec+1:
            print("%sNot the same number of eigs as nvec.%s" %
                  (Fore.RED, Fore.RESET))
            self.conn.Query("execute",
                            "UPDATE eliash SET status = 'error' WHERE"
                            " eliashid = %d" % self.id)
        else:
            print("%sThe eigenvalues for this calculation are %s%s." %
                  (Fore.CYAN, leadEigs, Fore.RESET))
            self.conn.Query("execute", "UPDATE eliash SET status = 'done',"
                            " eigs = '%s' WHERE eliashid = %d" %
                            (leadEigs, self.id))
        return True

    def SetIrreps(self, mesh, precision=5e-5):
        self.id_check()
        self.Load(True)
        nvec = self.cells["eliash.nvec"]

        if self.cells["eliash.status"] != "done":
            print("%sThe status of this calculation is not 'done'."
                  " Make sure it is using the SetEigs function.%s" %
                  (Fore.RED, Fore.RESET))

        irreps = ""
        for v in range(nvec):
            gap = self.LoadGap(mesh, str(v+1).zfill(2))

            irr = gap.returnIrrep(basis="SpinOrb", precision=precision)
            if len(irr) != 1:
                print("%sNumber of irreps are not one for this gap."
                      " Change precision?%s" %
                      (Fore.RED, Fore.RESET))
            irreps += "%s; " % re.sub('\$', '', irr[0])

        print("%sThe irreps of each gap of this calculation are %s%s." %
              (Fore.CYAN, irreps, Fore.RESET))
        self.conn.Query("execute", "UPDATE eliash SET status = 'done',"
                        " irreps = '%s' WHERE eliashid = %d" %
                        (irreps, self.id))
        return True

    def PlotEliashGap(self, mesh, params=None):
        if params is None:
            params = PlotParams()
        if self.id == -1:
            raise ValueError("Error: no id.")

        if not isinstance(params, PlotParams):
            raise TypeError("Error: params must be of type PlotParams.")

        if not isinstance(mesh, KptMesh):
            raise TypeError("Error: mesh can only be of type KptMesh.")

        if len(self.cells) == 0:
            self.cells = self.Load(True)

        if not self.cells['eliash.parity'] in ["0", "1"]:
            raise ValueError("ERROR: parity is", self.cells['eliash.parity'])

        norb = self.cells["barechi2.norb"]
        nfreqs = self.cells["eliash.liwn_max"] + 1
        nvec = self.cells["eliash.nvec"]

        default = PlotParams()
        default["subplots"] = [2, 3]
        default["indices"] = [[[0, 0], [4, 4], [8, 8]],
                              [[0, 0], [4, 4], [8, 8]]]
        default["freqs"] = range(nfreqs)
        default["vecs"] = range(nvec)
        default["figsize"] = (12, 8)
        default["title"] = ""
        default["titlesize"] = 14
        xlim = [0, 2*mesh.n[0]-1]
        ylim = [0, 2*mesh.n[1]-1]
        default["xlim"] = [[xlim, xlim, xlim, xlim], [xlim, xlim, xlim, xlim]]
        default["ylim"] = [[ylim, ylim, ylim, ylim], [ylim, ylim, ylim, ylim]]
        xlabs = [r"$\Im\{\Delta_{ [&l1], [&l2] }(\vec{q},\omega_{ [&w] })\}$",
                 r"$\Re\{\Delta_{ [&l1], [&l2] }(\vec{q},\omega_{ [&w] })\}$"]
        default["xlabels"] = [[xlabs, xlabs, xlabs, xlabs],
                              [xlabs, xlabs, xlabs, xlabs]]
        default["ylabels"] = None
        default["xticks"] = [None]
        default["yticks"] = [None]
        default["ticksize"] = 10
        default["cmap"] = [["bwr", "bwr", "bwr", "bwr"],
                           ["bwr", "bwr", "bwr", "bwr"]]
        default["bar_format"] = [["%01.01e", "%01.01e", "%01.01e", "%01.01e"],
                                 ["%01.01e", "%01.01e", "%01.01e", "%01.01e"]]
        default["bar_ticksize"] = 10

        default["tight_layout"] = True
        default["show"] = False
        default["savefig"] = None
        default["legend_loc"] = None
        default["special1"] = False
        default["special2"] = False

        default["sharey"] = False

        params.setDefaults(default)

        print(params.params)
        prtws = []
        for w in range(-nfreqs + 1, 0):
            prtws.append(str(w))
        prtws.append("-0")
        for w in range(0, nfreqs):
            prtws.append("+" + str(w))
        print("prtws : ", prtws)

        for vec in params["vecs"]:
            ReObject = PhysObj(norb, mesh.nkpt, 2*nfreqs, linear=True)
            ImObject = PhysObj(norb, mesh.nkpt, 2*nfreqs, linear=True)
            ReObject.Read("%s%sOutput/%s.ReGapFn%s" %
                          (self.conn.groundDir, self.baseDir,
                           self.system, str(vec + 1).zfill(2)), _skip=1)
            ImObject.Read("%s%sOutput/%s.ImGapFn%s" %
                          (self.conn.groundDir, self.baseDir,
                           self.system, str(vec + 1).zfill(2)), _skip=1)

            for freq in params["freqs"]:
                fig, ax = plotting.setFigure(params)

                wp = nfreqs + freq
                wm = nfreqs - 1 - freq

                for b in range(params["subplots"][1]):
                    l1 = params["labels"][params["indices"][0][b][0]]
                    l2 = params["labels"][params["indices"][0][b][1]]
                    printVariables = {"l1": l1, "l2": l2, "w": prtws[wp]}
                    plotting.plotGrid2(ax, 0, b, [ReObject, ImObject],
                                       wp, mesh, params, printVariables)
                    l1 = params["labels"][params["indices"][1][b][0]]
                    l2 = params["labels"][params["indices"][1][b][1]]
                    printVariables["w"] = prtws[wm]
                    plotting.plotGrid2(ax, 1, b, [ReObject, ImObject],
                                       wm, mesh, params, printVariables)

                plotting.setSubplot(ax, params)

                if params["savefig"] is None:
                    saveplace = None
                else:
                    saveplace = (self.conn.groundDir + self.baseDir +
                                 "GapFn%s_w%d%s" % (str(vec).zfill(2), freq,
                                                    params["savefig"]))
                plotting.finish(params, saveplace=saveplace)

    def PlotPairingVertex(self, mesh, plotParams=None):
        if self.id == -1:
            raise ValueError("Error: no id.")
        if plotParams is not None and not isinstance(plotParams, PlotParams):
            raise TypeError("Error: plotParams must be of type PlotParams.")
        if not isinstance(mesh, KptMesh):
            raise TypeError("Error: mesh must be of type KptMesh.")

        # nfreqs = self.cells["eliash.liwn_max"] + 1

        default = PlotParams()
        default["subplots"] = [2, 2]
        default["indices"] = [[[0, 0], [3, 3]],
                              [[1, 1], [2, 2]]]
        default["freqs"] = [0]
        default["figsize"] = (12, 8)
        default["title"] = ""
        default["titlesize"] = 14
        xlim = [0, 2*mesh.n[0]-1]
        ylim = [0, 2*mesh.n[1]-1]
        default["xlim"] = [[xlim, xlim], [xlim, xlim]]
        default["ylim"] = [[ylim, ylim], [ylim, ylim]]
        default["xlabels"] = [[None, None], [None, None]]
        default["ylabels"] = [[None, None], [None, None]]
        default["xticks"] = [[None, None], [None, None]]
        default["yticks"] = [[None, None], [None, None]]
        default["cmap"] = [["opt1", "opt1"], ["opt1", "opt1"]]
        default["bar_format"] = [["%01.01e", "%01.01e"],
                                 ["%01.01e", "%01.01e"]]
        default["bar_ticksize"] = 10
        default["tight_layout"] = False
        default["show"] = False
        default["legend_loc"] = None
        default["special1"] = False

        plotParams.setDefaults(default)

        for v in plotParams["freqs"]:
            fig, ax = plotting.setFigure(plotParams)

            Vertex_ph1 = self.LoadVertex_ph1(mesh.nkpt)

            for a, b in it.product(range(plotParams["subplots"][0]),
                                   range(plotParams["subplots"][1])):
                plotting.plotGrid(ax, b, a, Vertex_ph1, v, mesh, plotParams)

            plotting.plotLine(ax, [[0, mesh.n[0]-1],
                                   [mesh.n[1]-1, 2*mesh.n[1]-1]], plotParams)
            plotting.plotLine(ax, [[0, mesh.n[0]-1],
                                   [mesh.n[1]-1, 0]], plotParams)
            plotting.plotLine(ax, [[mesh.n[0]-1, 2*mesh.n[0]-1],
                                   [0, mesh.n[1]-1]], plotParams)
            plotting.plotLine(ax, [[mesh.n[0]-1, 2*mesh.n[0]-1],
                                   [2*mesh.n[1]-1, mesh.n[1]-1]], plotParams)

            plotting.setSubplot(ax, plotParams)

            if plotParams["special1"]:
                plotting.plotSpecial1(ax, plotParams["special1"], mesh.n,
                                      plotParams)

            saveplace = self.conn.groundDir + self.baseDir + "Vertex.png"
            plotting.finish(plotParams, saveplace)

    def PlotLadderFunction(self, nkpts, sgroup, ladder="ReVertexLadderph_1",
                           lls=None, plotParams=None):
        if plotParams is None:
            plotParams = {}
        if lls is None:
            lls = [[0, 0], [1, 1]]
        colorama.init()

        if self.id == -1:
            print("%sError: You cannot plot from this calculation because it"
                  " is not defined in the database. Please load it using"
                  " self.Load(id).%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        self.Load(all=True)

        Mesh = KptMesh(nkpts, _sgroup=sgroup)
        Mesh.read("%s%sInput/%s.klist" %
                  (self.conn.groundDir, self.baseDir, self.system))

        norb = int(self.cells["barechi2.norb"])

        if norb != 3:
            print("%sError: norb is %d, case not worked out yet.%s" %
                  (Fore.BLACK + Back.RED, norb, Style.RESET_ALL))
            return False
        else:
            ls = ["xy", "yz", "zx"]

        Ladder = PhysObj(norb**2, Mesh.nkpt, self.cells['eliash.livn'] + 1,
                         linear=False)
        if not Ladder.Read("%s%sOutput/%s.%s" %
                           (self.conn.groundDir, self.baseDir,
                            self.system, ladder), _skip=2):
            print("%sError: no %s file.%s" %
                  (Back.RED + Fore.BLACK, ladder, Style.RESET_ALL))
            return False

        Chi = PhysObj(norb**2, Mesh.nkpt, self.cells['eliash.livn'] + 1,
                      linear=False)
        if not Chi.Read("%s%sInput/%s.ReBareChipp" %
                        (self.conn.groundDir, self.baseDir, self.system),
                        _skip=2):
            print("%sError: no ReBareChipp file.%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False

        if "figsize" in plotParams:
            figsize = plotParams["figsize"]
        else:
            figsize = (8, 5)
        fig, ax = pl.subplots(len(lls), self.cells["eliash.livn"] + 1,
                              figsize=figsize)

        for i, ll in enumerate(lls):
            for v in range(self.cells['eliash.livn'] + 1):
                PlaneLadder = Ladder.ReturnInPlane(Mesh, [ll, v])
                ImgLadder = ax[i, v].imshow(PlaneLadder, origin="lower",
                                            cmap=pl.get_cmap("Blues"))

                print("Plotting v %d, max value %f" %
                      (v, pl.amax(PlaneLadder)))

                Ladder_ax = inset_axes.inset_axes(ax[i, v], width="3%",
                                                  height="90%", loc=2)
                Ladder_bar = pl.colorbar(ImgLadder, cax=Ladder_ax,
                                         format="%01.01e")
                Ladder_bar.ax.tick_params(labelsize=10)
                Ladder_bar.set_ticks([pl.amin(PlaneLadder),
                                      pl.amax(PlaneLadder)])

                ax[i, v].set_xticks([])
                ax[i, v].set_yticks([])
                if not i:
                    ax[0, v].set_title(r"$n = %d$" % v, fontsize=16)

                '''
                PlaneChi = Chi.ReturnInPlane(Mesh, [ll, v])
                ImgChi = ax[1,v].imshow(PlaneChi, origin = "lower",
                                        cmap = pl.get_cmap("Reds_r"))

                Chi_ax = inset_axes.inset_axes(ax[1, v], width = "3%",
                                               height="90%", loc = 2)
                Chi_bar = pl.colorbar(ImgChi, cax = Chi_ax, format = "%01.01e")
                Chi_bar.ax.tick_params(labelsize=10)
                Chi_bar.set_ticks([pl.amin(PlaneChi), pl.amax(PlaneChi)])

                ax[1, v].set_xticks([])
                ax[1, v].set_yticks([])
                '''

                if sgroup == 139:
                    ax[i, v].set_xlim([0, 2*nkpts[0]-1])
                    ax[i, v].set_ylim([0, 2*nkpts[1]-1])
                    '''
                    ax[1, v].set_xlim([0, 2*nkpts[0]-1])
                    ax[1, v].set_ylim([0, 2*nkpts[1]-1])
                    '''

                    ax[i, v].plot([0, nkpts[0]-1], [nkpts[1]-1, 2*nkpts[1]-1],
                                  color='black', linewidth=1, linestyle='--')
                    ax[i, v].plot([0, nkpts[0]-1], [nkpts[1]-1, 0],
                                  color='black', linewidth=1, linestyle='--')
                    ax[i, v].plot([nkpts[0]-1, 2*nkpts[0]-1], [0, nkpts[1]-1],
                                  color='black', linewidth=1, linestyle='--')
                    ax[i, v].plot([nkpts[0]-1, 2*nkpts[0]-1], [2*nkpts[1]-1,
                                                               nkpts[1]-1],
                                  color='black', linewidth=1, linestyle='--')

                    '''
                    ax[1, v].plot([0         , nkpts[0]-1]  , [nkpts[1]-1  ,
                    2*nkpts[1]-1], color='black', linewidth=1, linestyle='--')
                    ax[1, v].plot([0         , nkpts[0]-1]  , [nkpts[1]-1  , 0
                    ], color='black', linewidth=1, linestyle='--')
                    ax[1, v].plot([nkpts[0]-1, 2*nkpts[0]-1], [0
                    , nkpts[1]-1  ], color='black', linewidth=1,
                    linestyle='--')
                    ax[1, v].plot([nkpts[0]-1, 2*nkpts[0]-1], [2*nkpts[1]-1,
                    nkpts[1]-1  ], color='black', linewidth=1, linestyle='--')
                    '''

                    _a = 3.873
                    _b = _a
                    _c = 12.879

                    dx = nkpts[0]/2/math.cos(math.atan(_a/_c))
                    dy = nkpts[1]/2/math.cos(math.atan(_b/_c))

                    def f(x1, x2, y1, y2, x):
                        return 1.*(y2-y1)/(x2-x1)*(x-x1)+y1

                    def g(x1, x2, y1, y2, y):
                        return 1.*(x2-x1)/(y2-y1)*(y-y1)+x1

                    def f1(x):
                        return f(nkpts[0]-1, 2*nkpts[0]-1, 0, nkpts[1]-1, x)

                    def g1(y):
                        return g(nkpts[0]-1, 2*nkpts[0]-1, 0, nkpts[1]-1, y)

                    def f2(x):
                        return f(nkpts[0]-1, 2*nkpts[0]-1, 2*nkpts[1]-1,
                                 nkpts[1]-1, x)

                    def g2(y):
                        return g(nkpts[0]-1, 2*nkpts[0]-1, 2*nkpts[1]-1,
                                 nkpts[1]-1, y)

                    def f3(x):
                        return f(0, nkpts[0]-1, nkpts[1]-1, 0, x)

                    def g3(y):
                        return g(0, nkpts[0]-1, nkpts[1]-1, 0, y)

                    def f4(x):
                        return f(0, nkpts[0]-1, nkpts[1]-1, 2*nkpts[1]-1, x)

                    def g4(y):
                        return g(0, nkpts[0]-1, nkpts[1]-1, 2*nkpts[1]-1, y)

                    ax[i, v].plot([nkpts[0]-1+dx, nkpts[0]-1+dx],
                                  [f1(nkpts[0]-1+dx), f2(nkpts[0]-1+dx)],
                                  color='black', linewidth=1, linestyle='--')
                    ax[i, v].plot([nkpts[0]-1-dx, nkpts[0]-1-dx],
                                  [f3(nkpts[0]-1-dx), f4(nkpts[0]-1-dx)],
                                  color='black', linewidth=1, linestyle='--')
                    ax[i, v].plot([g3(nkpts[1]-1-dy), g1(nkpts[1]-1-dy)],
                                  [nkpts[1]-1-dy, nkpts[1]-1-dy],
                                  color='black', linewidth=1, linestyle='--')
                    ax[i, v].plot([g4(nkpts[1]-1+dy), g2(nkpts[1]-1+dy)],
                                  [nkpts[1]-1+dy, nkpts[1]-1+dy],
                                  color='black', linewidth=1, linestyle='--')

                    '''
                    ax[1, v].plot([nkpts[0]-1+dx, nkpts[0]-1+dx],
                    [f1(nkpts[0]-1+dx), f2(nkpts[0]-1+dx)],
                    color='black', linewidth=1, linestyle='--')
                    ax[1, v].plot([nkpts[0]-1-dx, nkpts[0]-1-dx],
                    [f3(nkpts[0]-1-dx), f4(nkpts[0]-1-dx)],
                    color='black', linewidth=1, linestyle='--')
                    ax[1, v].plot([g3(nkpts[1]-1-dy), g1(nkpts[1]-1-dy)],
                    [nkpts[1]-1-dy, nkpts[1]-1-dy],
                    color='black', linewidth=1, linestyle='--')
                    ax[1, v].plot([g4(nkpts[1]-1+dy), g2(nkpts[1]-1+dy)],
                    [nkpts[1]-1+dy, nkpts[1]-1+dy],
                    color='black', linewidth=1, linestyle='--')
                    '''

            print("norb: ", norb, "; ll[0]: ", ll[0], "; ll[1]: ", ll[1])
            ax[i, 0].set_ylabel("$l_1 = %s, l_2 = %s$\n$l_3 = %s, l_4 = %s$" %
                                (ls[int(ll[0] / norb)], ls[int(ll[0] % norb)],
                                 ls[int(ll[1] / norb)], ls[int(ll[1] % norb)]),
                                fontsize=16)
            r'''
            ax[1, 0].set_ylabel(r"$\Re\{[\chi_{pp}^0(0)]_"
                                r"{K,l_1,l_2;K,l_3,l_4}\}$", fontsize = 14)
            '''

        fig.text(0.06, 0.5, r"$\Re\{-\frac{1}{2}[\Phi_{ph}^d(i\nu_n, \vec{q})]"
                            r"_{l_1l_2;l_3l_4} + \frac{3}{2}[\Phi_{ph}^m"
                            r"(i\nu_n, \vec{q})]_{l_1l_2;l_3l_4}\}$",
                 ha='center', va='center',
                 rotation='vertical', fontsize=20)
        fig.text(0.5, 0.94, "Dominant Ladder Function Elements for Parity = %s"
                            ", Us = %s and Js = %s" %
                            (self.cells["eliash.parity"],
                             str(self.cells["dressed.dressedU"]),
                             str(self.cells["dressed.dressedJ"])),
                 ha="center", va="center", fontsize=20)

        l_text = ''
        for i, l in enumerate(lls):
            if not i:
                l_text += "%d-%d" % (l[0], l[1])
            else:
                l_text += "_%d-%d" % (l[0], l[1])

        savename = "%s%s%s_ll-%s.png" % (self.conn.groundDir, self.baseDir,
                                         ladder, l_text)
        print("Save figure: %s" % savename)
        pl.savefig(savename)
        if "show" in plotParams and plotParams["show"]:
            print("Show")
            pl.show()
        else:
            print("Draw")
            pl.draw()

    def PlotPairingFunction(self, nkpts, sgroup, lls=None,
                            plotParams=None):
        if lls is None:
            lls = [[0, 0], [1, 1]]
        if plotParams is None:
            plotParams = {}
        colorama.init()

        if self.id == -1:
            print("%sError: You cannot plot from this calculation because it"
                  " is not defined in the database. Please load it"
                  " using self.Load(id).%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        self.Load(all=True)

        Mesh = KptMesh(nkpts, _sgroup=sgroup)
        Mesh.read("%s%sInput/%s.klist" %
                  (self.conn.groundDir, self.baseDir, self.system))

        norb = int(self.cells["barechi2.norb"])

        if norb != 3:
            print("%sError: norb is %d, case not worked out yet.%s" %
                  (Fore.BLACK + Back.RED, norb, Style.RESET_ALL))
            return False
        else:
            ls = ["xy", "yz", "zx"]

        Ladderpp = [PhysObj(norb**2, Mesh.nkpt, self.cells['eliash.livn'] + 1,
                    linear=False, kptprob=True) for x in range(2)]
        if not Ladderpp[0].Read("%s%sOutput/%s.ReVertexLadderpp_1" %
                                (self.conn.groundDir, self.baseDir,
                                 self.system), _skip=2):
            print("%sError: no ReVertexLadderpp_1 file.%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False

        if not Ladderpp[1].Read("%s%sOutput/%s.ReVertexLadderpp_2" %
                                (self.conn.groundDir, self.baseDir,
                                 self.system), _skip=2):
            print("%sError: no ReVertexLadderpp_2 file.%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False

        import pylab as pl
        import mpl_toolkits.axes_grid1.inset_locator as inset_axes

        for L in range(2):
            if "figsize" in plotParams:
                figsize = plotParams["figsize"]
            else:
                figsize = (8, 5)
            fig, ax = pl.subplots(len(lls), self.cells["eliash.livn"] + 1,
                                  figsize=figsize)

            for i, ll in enumerate(lls):
                for v in range(self.cells['eliash.livn'] + 1):
                    PlaneLadder = Ladderpp[L].ReturnInPlane(Mesh, [ll, v])
                    ImgLadder = ax[i, v].imshow(PlaneLadder, origin="lower",
                                                cmap=pl.get_cmap("Blues"))

                    print("Plotting v %d, max value %f" %
                          (v, pl.amax(PlaneLadder)))

                    Ladder_ax = inset_axes.inset_axes(ax[i, v], width="3%",
                                                      height="90%", loc=2)
                    Ladder_bar = pl.colorbar(ImgLadder, cax=Ladder_ax,
                                             format="%01.01e")
                    Ladder_bar.ax.tick_params(labelsize=10)
                    Ladder_bar.set_ticks([pl.amin(PlaneLadder),
                                          pl.amax(PlaneLadder)])

                    ax[i, v].set_xticks([])
                    ax[i, v].set_yticks([])
                    if not i:
                        ax[0, v].set_title(r"$n = %d$" % v, fontsize=16)

                    if sgroup == 139:
                        ax[i, v].set_xlim([0, 2*nkpts[0]-1])
                        ax[i, v].set_ylim([0, 2*nkpts[1]-1])

                        ax[i, v].plot([0, nkpts[0]-1], [nkpts[1]-1,
                                                        2*nkpts[1]-1],
                                      color='black', linewidth=1,
                                      linestyle='--')
                        ax[i, v].plot([0, nkpts[0]-1], [nkpts[1]-1, 0],
                                      color='black', linewidth=1,
                                      linestyle='--')
                        ax[i, v].plot([nkpts[0]-1, 2*nkpts[0]-1],
                                      [0, nkpts[1]-1], color='black',
                                      linewidth=1, linestyle='--')
                        ax[i, v].plot([nkpts[0]-1, 2*nkpts[0]-1],
                                      [2*nkpts[1]-1, nkpts[1]-1],
                                      color='black', linewidth=1,
                                      linestyle='--')

                        _a = 3.873
                        _b = _a
                        _c = 12.879

                        dx = nkpts[0]/2/math.cos(math.atan(_a/_c))
                        dy = nkpts[1]/2/math.cos(math.atan(_b/_c))

                        def f(x1, x2, y1, y2, x):
                            return 1.*(y2-y1)/(x2-x1)*(x-x1)+y1

                        def g(x1, x2, y1, y2, y):
                            return 1.*(x2-x1)/(y2-y1)*(y-y1)+x1

                        def f1(x):
                            return f(nkpts[0]-1, 2*nkpts[0]-1, 0, nkpts[1]-1,
                                     x)

                        def g1(y):
                            return g(nkpts[0]-1, 2*nkpts[0]-1, 0, nkpts[1]-1,
                                     y)

                        def f2(x):
                            return f(nkpts[0]-1, 2*nkpts[0]-1, 2*nkpts[1]-1,
                                     nkpts[1]-1, x)

                        def g2(y):
                            return g(nkpts[0]-1, 2*nkpts[0]-1, 2*nkpts[1]-1,
                                     nkpts[1]-1, y)

                        def f3(x):
                            return f(0, nkpts[0]-1, nkpts[1]-1, 0, x)

                        def g3(y):
                            return g(0, nkpts[0]-1, nkpts[1]-1, 0, y)

                        def f4(x):
                            return f(0, nkpts[0]-1, nkpts[1]-1, 2*nkpts[1]-1,
                                     x)

                        def g4(y):
                            return g(0, nkpts[0]-1, nkpts[1]-1, 2*nkpts[1]-1,
                                     y)

                        ax[i, v].plot([nkpts[0]-1+dx, nkpts[0]-1+dx],
                                      [f1(nkpts[0]-1+dx), f2(nkpts[0]-1+dx)],
                                      color='black', linewidth=1,
                                      linestyle='--')
                        ax[i, v].plot([nkpts[0]-1-dx, nkpts[0]-1-dx],
                                      [f3(nkpts[0]-1-dx), f4(nkpts[0]-1-dx)],
                                      color='black', linewidth=1,
                                      linestyle='--')
                        ax[i, v].plot([g3(nkpts[1]-1-dy), g1(nkpts[1]-1-dy)],
                                      [nkpts[1]-1-dy, nkpts[1]-1-dy],
                                      color='black', linewidth=1,
                                      linestyle='--')
                        ax[i, v].plot([g4(nkpts[1]-1+dy), g2(nkpts[1]-1+dy)],
                                      [nkpts[1]-1+dy, nkpts[1]-1+dy],
                                      color='black', linewidth=1,
                                      linestyle='--')

                print("norb: ", norb, "; ll[0]: ", ll[0], "; ll[1]: ", ll[1])
                ax[i, 0].set_ylabel("$l_1 = %s, l_2 = %s$\n$l_3 = %s,"
                                    " l_4 = %s$" %
                                    (ls[int(ll[0] / norb)],
                                     ls[int(ll[0] % norb)],
                                     ls[int(ll[1] / norb)],
                                     ls[int(ll[1] % norb)]), fontsize=16)

            fig.text(0.06, 0.5,
                     r"$\Re\{[\Phi^{pp}_%d(K'-K)]_{l_1l_2;l_3l_4}\}$" % (L+1),
                     ha='center', va='center', rotation='vertical',
                     fontsize=20)
            fig.text(0.5, 0.94,
                     "PP%d Ladder Function Elements for Parity"
                     " = %s, Us = %s and Js = %s" %
                     (L+1, self.cells["eliash.parity"],
                      str(self.cells["dressed.dressedU"]),
                      str(self.cells["dressed.dressedJ"])),
                     ha="center", va="center", fontsize=20)

            l_text = ''
            for i, l in enumerate(lls):
                if not i:
                    l_text += "%d-%d" % (l[0], l[1])
                else:
                    l_text += "_%d-%d" % (l[0], l[1])

            savename = ("%s%sReVertexLadderpp%d_ll-%s.png" %
                        (self.conn.groundDir, self.baseDir, L+1, l_text))
            print("Save figure: %s" % savename)
            pl.savefig(savename)
            if "show" in plotParams and plotParams["show"]:
                print("Show")
                pl.show()
            else:
                print("Draw")
                pl.draw()
