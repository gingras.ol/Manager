import os
import re
# from manager.MySQL import mysql
from manager.PostProd import KptMesh
from manager.sql_db import Connector
import subprocess
import shutil
import colorama
from colorama import Fore, Back, Style

import readline
from manager.Completer import Completer


class Calculation():
    def __init__(self, system, conn=None):
        # System related variables
        self.system = system

        # Database related variables
        self.table = ""
        self.id = -1
        self.cells = {}
        self.params = {}
        self.paramsSet = False

        self.funcParams = {}

        # Previous calculations parameters, useful when starting a calculation
        # from a previous one; disabled if len(self.prevTable) == 0
        self.prevTable = []
        self.prevId = -1
        self.prevCalc = None

        # Directory related variables
        self.tree = {}
        self.treeDir = ""
        self.baseDir = ""

        # Starts the text coloration package
        colorama.init()
        # Starting a connection with the database
        # self.conn = mysql.Connector(_side=os.environ["mysql_user"])
        self.conn = conn

    def testConn(self):
        """Tests if self.conn is well-defined."""
        if self.conn is None:
            raise Exception("Cannot create a database table for Calculation"
                            " if there is no database connection")
        elif not isinstance(self.conn, Connector):
            raise Exception("The database connector is badly defined."
                            "Should be of type Connector.")

    def id_check(self):
        if self.id == -1:
            mess = ("%sError: You cannot find leading eigenvalues"
                    " from this calculation since it has no defined "
                    "id%s" % (Back.RED + Fore.BLACK, Style.RESET_ALL))
            raise ValueError(mess)

    # Function to load a calculation from the database
    def Load(self, all=False):
        # If the object is not associated to an id in the database,
        # print Error and leave
        if self.id == -1:
            print("%sError: Cannot call Calculation.Load if self.id is -1%s" %
                  (Fore.BLACK + Back.RED, Style.RESET_ALL))
            return {}

        # Print that we load a calculation with its id and table
        print("%sLoading calculation %s%sid is %d%s from the database%s" %
              (Fore.CYAN, Fore.BLACK + Back.BLUE + Style.BRIGHT, self.table,
               self.id, Style.RESET_ALL + Fore.CYAN, Style.RESET_ALL))

        # Taking the column names of the table
        select_table = self.table
        # tableName = "table_name = '%s'" % self.table
        tables = [self.table] + self.prevTable

        columns_query = "PRAGMA table_info(%s)" % self.table
        columns_ret = self.conn.Query("select", columns_query)
        select_values = ''

        fields = []
        for c, col in enumerate(columns_ret):
            fields.append("%s.%s" % (self.table, col[1]))
            if c:
                select_values += ', '
            select_values += '%s' % fields[-1]

        if all:
            for i in range(1, len(tables)):
                select_table += (" JOIN %s ON %s.%sid = %s.%sid" %
                                 (tables[i], tables[i], tables[i], tables[i-1],
                                  tables[i]))
                columns_query = "PRAGMA table_info(%s)" % tables[i]
                columns_ret = self.conn.Query("select", columns_query)
                for col in columns_ret:
                    fields.append("%s.%s" % (tables[i], col[1]))
                    select_values += ', %s' % fields[-1]

        select_where = "%s.%sid = %d" % (self.table, self.table, self.id)

        request = self.conn.Query("select",
                                  "SELECT %s FROM %s WHERE %s" %
                                  (select_values, select_table, select_where))
        # If no id defined, print Error and leave
        if not request:
            print("%sError: This id (%d) doesn't exist for this table (%s)%s" %
                  (Fore.BLACK + Back.RED, self.id, self.table,
                   Style.RESET_ALL))
            return {}

        cells = {}
        # Find the base directory of the calculation
        for i, f in enumerate(fields):
            if f == "%s.%sdir" % (self.table, self.table):
                self.baseDir = request[0][i]
            if len(self.prevTable):
                if f == self.prevTable[0] + "." + self.prevTable[0] + "id":
                    self.prevId = request[0][i]
            cells[f] = request[0][i]

        # Print the loading is done and return data
        print("%sCalculation well loaded%s\n" % (Fore.CYAN, Style.RESET_ALL))
        self.cells = cells

        # if self.treeDir != "":
        #         def mysqlText(text):
        #             text = re.sub(r"\.", r"\.", text)
        #             return text
        #         # Loop on previous datas
        #         for p in self.cells:
        #             if re.search(r"&\(%s\)" % mysqlText(p), self.treeDir):
        #                 if self.cells[p] is None:
        #                     print("%sError: Some data from the previous"
        #                           " calculation"
        #                           " are None%s" % (Back.RED + Fore.BLACK,
        #                                            Style.RESET_ALL))
        #                     return False
        #                 # Replace in string
        #                 self.treeDir = re.sub(r"&\(%s\)" % mysqlText(p),
        #                                       str(self.cells[p]),
        #                                       self.treeDir)
        #         if len(self.funcParams):
        #             for fP in self.funcParams:
        #                 print("funcParams: fP = %s", fP)
        #                 self.treeDir = re.sub(r"!\(%s\)" % mysqlText(fP),
        #                                       self.funcParams[fP](),
        #                                       self.treeDir)
        #         print("%sSetted self.treeDir: %s%s" %
        #               (Fore.CYAN, self.treeDir, Fore.RESET))
        return True

    def LoadKptMesh(self, _grid, _sgroup, _type=None, _file=None):
        if self.id == -1:
            raise("Error: No id.")

        if _file is None:
            _file = ("%s%sInput/%s.klist" %
                     (self.conn.groundDir, self.baseDir,
                      self.system))

        return KptMesh.read(_file, _grid, _sgroup, _type=_type)

    def SetSbatch(self, params, launch=False, force=False):
        sbatchfilePath = re.match(r"^(.+/)\w+\.\w+$",
                                  os.path.realpath(__file__)).group(1)
        print("Making Sbatch file")
        with open("%ssbatchFile.dat" % sbatchfilePath) as f:
            fileInput = f.read()

        if "filePath" not in params:
            print("%sError: Please define params['filePath'].%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False
        else:
            filePath = params["filePath"]
            sbatchPath = re.match(r"^(.+/)\w+\.\w+$", filePath).group(1)
            print("File path: %s" % filePath)

        for p in ["job-name", "account", "ntasks", "mem-per-cpu", "time",
                  "do"]:
            if p not in params:
                print("%sError: Please define params['%s'].%s" %
                      (Back.RED + Fore.BLACK, p, Style.RESET_ALL))
                return False
            else:
                fileInput = re.sub(r"!\(%s\)" % p, params[p], fileInput)
                print("%s:" % p, params[p])

        if "module" in params:
            print("module: %s" % params["module"])
            fileInput = re.sub(r"!\(module\)", params["module"], fileInput)
        else:
            fileInput = re.sub(r"!\(module\)", "# No modules", fileInput)

        if not os.path.exists(filePath):
            with open(filePath, "w") as f:
                f.write(fileInput)
        else:
            if not force:
                print("%s\tAlready exists, you can use force ="
                      " True to overwrite.%s" %
                      (Fore.MAGENTA + Style.BRIGHT, Style.RESET_ALL))
            else:
                os.remove(filePath)
                with open(filePath, "w") as f:
                    f.write(fileInput)

        if launch is True:
            self.Launch(sbatchPath, filePath, "sbatch", force)
        return True

    def Launch(self, subPath, filePath, prog, force=False):
        os.chdir(subPath)
        if self.cells['%s.status' % self.table] != 'ready' and not force:
            print("%sError: This calculation is not ready to launch."
                  " Abort launching.%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False

        os.chdir(subPath)
        subprocess.call("%s %s" % (prog, filePath), shell=True)
        self.DatabaseAction(status="launched")
        return True

    # Function that fills the necessary parameters for creating the tree,
    # making file and making links
    def SetParams(self, params=None, verify=True):
        if params is None:
            params = {}
        # Skip it all if verify is False
        if not verify:
            print("\n%sParameters given%s" % (Fore.CYAN, Fore.RESET))
            self.params = params
            self.paramsSet = True
            return True

        # If params was feed, is it compatible?
        if len(params):
            # Different length: incompatible
            if len(params) != len(self.params):
                print("%sError: There are not as many params given to the"
                      " function SetParams() as the number of self.params%s" %
                      (Back.RED + Fore.BLACK, Style.RESET_ALL))
                return False
            # Scan through all params
            for p in params:
                # Not a param
                if p not in self.params:
                    print("%sError: The params given to the function"
                          " SetParams() are not compatible with"
                          " self.params%s" %
                          (Back.RED + Fore.BLACK, Style.RESET_ALL))
                    return False
                # Undefined
                if params[p] is None:
                    print("%sError: The params giver to the function"
                          " SetParams() contain a None value. All params"
                          " need to be defined%s" %
                          (Back.RED + Fore.BLACK, Style.RESET_ALL))
                    return False
            # All is good
            self.params = params
            self.paramsSet = True
            return True

        if self.id != -1:
            for p in self.params:
                self.params[p] = self.cells["%s.%s" % (self.table, p)]
            self.paramsSet = True
            return True

        # Ask for all params
        print("%sInput requiered:%s\t%sSetting the calculation's"
              " parameters%s" %
              (Back.GREEN + Fore.BLACK, Style.RESET_ALL, Fore.GREEN,
               Fore.RESET))
        for p in self.params:
            default = ""
            if self.params[p]:
                default = str(self.params[p])
            self.params[p] = input("\t%s>>> %s%s? %s %s\t" %
                                   (Fore.GREEN, Style.BRIGHT, p, default,
                                    Style.RESET_ALL) or default)
            print(self.params[p])
        self.ParamsSet = True
        return True

    # Usually used to insert calculation in the database,
    # also used to just change the status in the database of an
    # existing calculation
    def DatabaseAction(self, status=None, clean=False):
        # If we just change a status
        if status:
            print("%sChanging status in the database to %s%s" %
                  (Fore.RED, status, Fore.RESET))
            self.conn.Query("execute",
                            "UPDATE %s SET status = '%s' WHERE %sid = %d" %
                            (self.table, status, self.table, self.id))
            return True

        # Check whether there is a defined base directory
        if self.baseDir == "":
            print("%sError: No base directory selected.%s" %
                  (Fore.BLACK + Back.RED, Style.RESET_ALL))
            return False

        # Check whether params are set
        if self.paramsSet is False:
            print("%sError: Params are not set.%s" %
                  (Fore.BLACK + Back.RED, Style.RESET_ALL))
            return False

        if self.id != -1:
            finalDir = self.cells["%s.%sdir" % (self.table, self.table)]
        else:
            finalDir = self.baseDir + self.treeDir

        request = self.conn.Query("select",
                                  ("SELECT COUNT(*) FROM %s WHERE"
                                   " %sdir = '%s'" %
                                   (self.table, self.table, finalDir)))

        if request[0][0] == 1:
            print("%sThis directory is already registered as a calculation.%s")
            if clean or input("%sInput requiered:%s %sDo you want to reset"
                              " this directory in the database? (y/[n])%s" %
                              (Back.GREEN + Fore.BLACK, Style.RESET_ALL,
                               Fore.GREEN, Fore.RESET)) == "n":
                if self.id == -1:
                    self.conn.Query("execute",
                                    "DELETE FROM %s WHERE %sdir = '%s'" %
                                    (self.table, self.table, finalDir))
                else:
                    print("Nothing happends")
                    return True
        elif request[0][0] == 0:
            pass
        else:
            if clean or input("%sInput requiered:%s %sThis directory is taken"
                              " multiple times. Erase all from %s? (y/[n])%s" %
                              (Back.GREEN + Fore.BLACK, Style.RESET_ALL,
                               Fore.GREEN, finalDir, Style.RESET_ALL)) == "y":
                print("%sErasing all %s%s" % (Fore.RED, finalDir, Fore.RESET))
                self.conn.Query("execute",
                                "DELETE FROM %s WHERE %sdir = '%s'" %
                                (self.table, self.table, finalDir))
#            self.id = self.conn.Query("select",
#                                      "SELECT %sid from %s"
#                                      " WHERE %sdir = '%s'" %
#                                      (self.table, self.table, self.table,
#                                       self.baseDir))[0][0]
            return False

        # If not in the database, prepare to insert
        fields = "(status, %sdir" % self.table
        values = "('makeTree', '%s'" % finalDir

        if self.prevId != -1:
            fields += ", %sid" % self.prevTable[0]
            values += ", %d" % self.prevId

        for p in self.params:
            if self.params[p] is None:
                print("%sError: Cannot insert in the database: a"
                      " parameter is None.%s" %
                      (Back.RED + Fore.BLACK, Style.RESET_ALL))
                return False
            if isinstance(self.params[p], str):
                param = "'%s'" % self.params[p]
            else:
                param = self.params[p]
            fields += ", %s" % p
            values += ", %s" % param

        fields += ")"
        values += ")"

        print("%sInserting the calculation in the database%s" %
              (Fore.BLACK + Back.BLUE, Style.RESET_ALL))
        self.id = self.conn.Query("insert",
                                  "INSERT INTO %s %s VALUES %s" %
                                  (self.table, fields, values))
        print("%sCalculation inserted in table %s with id %d%s" %
              (Fore.CYAN, self.table, self.id, Fore.RESET))
        return True

    # Set manually the base directory, useful for creating a calculation
    def SetBaseDir(self, baseDir=""):
        # Prints the ground directory
        print("\n%sChanging base directory of the calculation."
              " Current ground directory is %s%s%s" %
              (Fore.CYAN, Style.BRIGHT + Fore.BLACK + Back.BLUE,
               self.conn.groundDir, Style.RESET_ALL))

        # If base directory is given as input, just set it,
        # else there is a procedure
        if baseDir != "":
            self.baseDir = baseDir
        else:
            # To set baseDir, we use self.ScanGroundDir(),
            # but we can help the user chose the right one
            # If there is no prevTable, just ask for baseDir
            if not len(self.prevTable):
                self.baseDir = self.ScanGroundDir()
            # If there is a prevTable but no prevId, there was a
            # mistake because we ask that first
            elif self.prevId == -1:
                print("%sError: Before setting a base directory,"
                      " link this calculation to the previous"
                      " one in table %s%s" %
                      (Fore.BLACK + Back.RED, self.prevTable[0],
                       Style.RESET_ALL))
                return False
            else:
                # Select all calculation with same prevId and
                # baseDir should be the same
                request = self.conn.Query("select",
                                          "SELECT %sdir FROM %s WHERE"
                                          " %sid = %d" %
                                          (self.table, self.table,
                                           self.prevTable[0], self.prevId))
                # If no other calculation with same prevId, just ask baseDir
                if not request:
                    print("%sFirst calculation associated to %sid = %d%s" %
                          (Fore.CYAN, self.prevTable[0], self.prevId,
                           Style.RESET_ALL))
                    self.baseDir = self.ScanGroundDir()
                else:
                    # Else check the distinct baseDir
                    # used in other calculations
                    baseDirs = []
                    for r in request:
                        if not r[0] in baseDirs:
                            baseDirs.append(r[0])

                    # There should usually be just one different baseDir
                    if len(baseDirs) == 1:
                        print("%sOther calculations from %s table uses the"
                              " same previous %sid = %d and have the following"
                              " base directory: %s%s" %
                              (Fore.CYAN, self.table, self.prevTable[0],
                               self.prevId, request[0][0], Style.RESET_ALL))
                        # Does the user wants this one?
                        if input("%sInput requiered:%s\t%sTake the same?"
                                 " ([y]/n)%s\t" %
                                 (Back.GREEN + Fore.BLACK, Back.RESET,
                                  Fore.GREEN, Fore.RESET)) != "n":
                            self.baseDir = request[0][0] + "/"
                        else:
                            self.baseDir = self.ScanGroundDir()
                    # If there are many different baseDirs, print them and
                    # ask the user to chose (Right now no sublist, just choose
                    # among all, could be changed)
                    else:
                        print("%sMany differents base directories for"
                              " different calculations, TO DO %s" %
                              (Fore.BLACK + Back.RED, Style.RESET_ALL))
                        # mysql.PrintSelect([baseDirs], [["baseDir"]])
                        self.conn.PrintSelect([baseDirs], [["baseDir"]])
                        self.baseDir = self.ScanGroundDir()

        # Print new base directory
        print("%sNew base directory for this calculation is %s%s%s" %
              (Fore.CYAN, Fore.BLACK + Back.BLUE, self.baseDir,
               Style.RESET_ALL))
        if not os.path.exists(self.conn.groundDir + self.baseDir):
            print("\t%sThis directory doesn't exist, creating it.%s" %
                  (Fore.CYAN + Style.BRIGHT, Style.RESET_ALL))
            os.mkdir(self.conn.groundDir + self.baseDir)

    # Function that sets the "tab" key for scaning groundDir
    def ScanGroundDir(self):
        completer = Completer(self.conn.groundDir)
        readline.set_completer(completer.complete)
        readline.set_completer_delims(' ')
        readline.parse_and_bind("tab: complete")

        return input("%sInput requiered: %s\t%sWhat is the"
                     " calculation's base directory?%s\t" %
                     (Back.GREEN + Fore.BLACK, Style.RESET_ALL,
                      Fore.GREEN, Style.RESET_ALL))

    # For a new calculation, starting from the basic tree,
    # construct the folders, files and links
    def MakeTree(self, force=False, clean=False):
        '''
        Requiered:
            self.tree       defined for each subclasses;
            self.baseDir    defined for each specific calculations;

        Function: Creates the folders defined in the tree directory.
        '''
        # Check if table and tree are defined and if no id is
        # associated to the calculation
        if self.table == "":
            print("%sError: Cannot MakeTree from a plain"
                  " Calculation object%s" %
                  (Fore.BLACK + Back.RED, Style.RESET_ALL))
            return False
        elif self.tree == {}:
            print("%sError: Cannot MakeTree if self.tree is not defined%s" %
                  (Fore.BLACK + Back.RED, Style.RESET_ALL))
            return False
        elif self.id != -1:
            if not clean:
                print("%sError: Cannot MakeTree for an existing calculation,"
                      " option not yet available%s" %
                      (Fore.BLACK + Back.RED, Style.RESET_ALL))
                return False

        # If the calculation needs a previous id, ask it, then load it
        prevData = {}
        if self.id != -1:
            prevData = self.cells
        elif len(self.prevTable) and self.prevId == -1:
            print("\n%sThis type of calculation requiers a previous"
                  " calculation of type %s%s" %
                  (Fore.CYAN, self.prevTable[0], Style.RESET_ALL))
            prevBaseDir = self.ScanGroundDir()

            request = self.conn.Query("select",
                                      "SELECT %sid FROM %s WHERE"
                                      " %sdir = '/%s'" %
                                      (self.prevTable[0], self.prevTable[0],
                                       self.prevTable[0], prevBaseDir[0:-1]))
            if not request:
                print("%sError: Couldn't associate this directory to a"
                      " calculation of the table %s%s" %
                      (Fore.BLACK + Back.RED, self.prevTable[0],
                       Style.RESET_ALL))
                return False
            elif len(request) > 1:
                print("%sError: Many possible ids for this base"
                      " directory, not treated yet%s" %
                      (Fore.BLACK + Back.RED, Style.RESET_ALL))
                return False
            else:
                self.prevId = request[0][0]
                print("%sThe corresponding calculation has the %sid = %d%s" %
                      (Fore.CYAN, self.prevTable[0], self.prevId,
                       Style.RESET_ALL))
                self.prevCalc.id = self.prevId
                if not self.prevCalc.Load(True):
                    print("%sError: Could not load the calculations.%s" %
                          (Fore.BLACK + Back.RED, Style.RESET_ALL))
                    return False

                prevData = {data: self.prevCalc.cells[data]
                            for data in self.prevCalc.cells}
#                for d, data in enumerate(self.prevCalc.cells[0]):
#                    prevData[data] = self.prevCalc.cells[1][d]
        elif self.prevId != -1:
            if not self.prevCalc.Load(True):
                print("%sError: Could not load the calculations.%s" %
                      (Fore.BLACK + Back.RED, Style.RESET_ALL))
                return False
            prevData = {data: self.prevCalc.cells[data]
                        for data in self.prevCalc.cells}
#            for d, data in enumerate(self.prevCalc.cells[0]):
#                prevData[data] = self.prevCalc.cells[1][d]

        # If baseDir not defined, ask it
        if self.baseDir == "":
            self.SetBaseDir()
        if self.baseDir == "":
            print("%sDid not select a baseDirectory, aborting%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False

        # Ask all the parameters of the calculation
        if not self.paramsSet:
            self.SetParams()

        # Function recursively used to clear the tree for parameters
        # and previous calculation's datas
        def CleanTree(dir):
            newDir = {}

            def mysqlText(text):
                text = re.sub(r"\.", r"\.", text)
                return text
            for elem in dir:
                # If element of directory is not a directory it-self, treat
                if not re.search(".+/$", elem):
                    # Copy the dictionary's entries
                    newElem = elem
                    newData = dir[elem]

                    # Loop to change parameters
                    for p in self.params:
                        # If parameter is None, it was badly
                        # set by SetParams() function
                        if self.params[p] is None:
                            print("%sError: Some parameters of the calculation"
                                  " are None, bad SetParams()%s" %
                                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
                            return False
                        # Replace params in strings for both label of"
                        # dictionary and value
                        print("Params: mysqlText(p) = %s, str(self.params[p])"
                              " = %s, newData = %s" %
                              (mysqlText(p), str(self.params[p]), newData))
                        newElem = re.sub(r"\$\(%s\)" % mysqlText(p),
                                         str(self.params[p]), newElem)
                        newData = re.sub(r"\$\(%s\)" % mysqlText(p),
                                         str(self.params[p]), newData)
                    # Loop to change previous calculation's datas
                    for p in prevData:
                        # Badly defined
                        if re.search(r"&\(%s\)" % mysqlText(p),
                                     newElem) or re.search(r"&\(%s\)" % str(p),
                                                           newData):
                            if prevData[p] is None:
                                print("%sError: Some data from the previous"
                                      " calculation are None%s" %
                                      (Back.RED + Fore.BLACK, Style.RESET_ALL))
                                print(p)
                                return False
                            # Replace in string
                            newElem = re.sub(r"&\(%s\)" % mysqlText(p),
                                             str(prevData[p]), newElem)
                            newData = re.sub(r"&\(%s\)" % mysqlText(p),
                                             str(prevData[p]), newData)
                    if len(self.funcParams):
                        for fP in self.funcParams:
                            newElem = re.sub(r"!\(%s\)" % mysqlText(fP),
                                             self.funcParams[fP](), newElem)
                            newData = re.sub(r"!\(%s\)" % mysqlText(fP),
                                             self.funcParams[fP](), newData)

                # If it is a directory, treat its dictionary with ClearTree()
                # function, and label the same way as other case
                else:
                    # Copy
                    newElem = elem
                    newData = CleanTree(dir[elem])
                    # If the result of cleaning a directory is an empty string,
                    # there was an error, return error
                    if newData == "":
                        print("%sError: Could not make a directory%s" %
                              (Back.RED + Fore.BLACK, Style.RESET_ALL))
                        return False

                    # Loop on parameters
                    for p in self.params:
                        if self.params[p] is None:
                            print("%sError: Some parameters of the calculation"
                                  " are None, bad SetParams()%s" %
                                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
                            return False
                        newElem = re.sub(r"\$\(%s\)" % mysqlText(p),
                                         str(self.params[p]), newElem)
                    # Loop on previous datas
                    for p in prevData:
                        if re.search(r"&\(%s\)" % mysqlText(p), newElem):
                            if prevData[p] is None:
                                print("%sError: Some data from the previous"
                                      " calculationn are None%s" %
                                      (Back.RED + Fore.BLACK, Style.RESET_ALL))
                                return False
                            # Replace in string
                            newElem = re.sub(r"&\(%s\)" % mysqlText(p),
                                             str(prevData[p]), newElem)
                    if len(self.funcParams):
                        for fP in self.funcParams:
                            print("funcParams: fP = %s", fP)
                            newElem = re.sub(r"!\(%s\)" % mysqlText(fP),
                                             self.funcParams[fP](), newElem)

                # Construct this entry of the new dictionary
                newDir[newElem] = newData
            # Return Directory
            return newDir

        # Function used recursively to construct the tree
        def MakeDir(dir, path, tab="\t"):
            def CleanFile(text):
                for p in self.params:
                    if self.params[p] is None:
                        print("%sError: Some parameters of the calculation are"
                              " None, bad SetParams()%s" %
                              (Back.RED + Fore.BLACK, Style.RESET_ALL))
                        return ""
                    text = re.sub(r"\$\(%s\)" % str(p),
                                  str(self.params[p]), text)
                # Loop on previous datas
                for p in prevData:
                    if re.search(r"&\(%s\)" % str(p), text):
                        if prevData[p] is None:
                            print("%sError: Some data from the previous"
                                  " calculation are None%s" %
                                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
                            return ""
                        # Replace in string
                        text = re.sub(r"&\(%s\)" % str(p),
                                      str(prevData[p]), text)
                if len(self.funcParams):
                    for fP in self.funcParams:
                        text = re.sub(r"!\(%s\)" % fP,
                                      self.funcParams[fP](), text)
                return text

            for elem in dir:
                # Test for a directory type, make it if it is and recursively
                # make it
                if re.match(".+/$", elem):
                    print("%s%s>>> Making directory %s%s" %
                          (tab, Fore.MAGENTA, path + elem, Style.RESET_ALL))
                    if not os.path.exists(path + elem):
                        os.mkdir(path + elem)
                    else:
                        print("%s%s\tAlready exists%s" %
                              (tab, Fore.MAGENTA + Style.BRIGHT,
                               Style.RESET_ALL))
                    if not MakeDir(dir[elem], path + elem, tab + "\t"):
                        return False
                    continue

                # Test for a file
                if re.search(r"^\w+\.\w+$", elem):
                    print("%s%s>>> Making file %s%s" %
                          (tab + "\t", Fore.MAGENTA, elem, Style.RESET_ALL))
                    if not os.path.exists(path + elem):
                        with open(path + elem, 'w') as f:
                            f.write(CleanFile(dir[elem]))
                    else:
                        print("%s%s\tAlready exists%s" %
                              (tab, Fore.MAGENTA + Style.BRIGHT,
                               Style.RESET_ALL))
                    continue

                # Test for a link
                link = re.match(r"^\[(.+)\]$", elem)
                if link:
                    print("%s%s>>> Making link %s from" %
                          (tab + "\t", Fore.MAGENTA, link.group(1)))
                    print("%s\t%s%s" % (tab + "\t", dir[elem],
                          Style.RESET_ALL))
                    if not os.path.exists(path + link.group(1)):
                        if not os.path.exists(self.conn.groundDir + dir[elem]):
                            print("%s\t%sError: Linked file does"
                                  " not exists%s" %
                                  (tab + "\t", Back.RED + Fore.BLACK,
                                   Style.RESET_ALL))
                            return False
                        os.symlink(self.conn.groundDir + dir[elem],
                                   path + link.group(1))
                    else:
                        print("%s\t%sAlready exists%s" %
                              (tab, Fore.MAGENTA + Style.BRIGHT,
                               Style.RESET_ALL))
                    continue

                # Nothing left
                print("%sError: Unrecognised type of tree element%s" %
                      (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return True

        # Cleaning the tree first, from parameters
        # and previous calculation's datas
        newTree = CleanTree(self.tree)
        # Check if the tree could be clean
        if not newTree:
            print("%sError: Could not clean the tree%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return ""

        # Go throught the tree
        print("\n%sMaking the tree%s" %
              (Back.MAGENTA + Fore.BLACK, Style.RESET_ALL))
        # Make the base directory if not yet existing
        if not os.path.exists(self.conn.groundDir + self.baseDir):
            print("%sBase directory does not exists, making it at %s%s%s" %
                  (Fore.CYAN, Style.BRIGHT,
                   self.conn.groundDir + self.baseDir, Style.RESET_ALL))
            os.mkdir(self.conn.groundDir + self.baseDir)

        # Making tree directory
        if self.id == -1 and self.treeDir != "":
            def makeTreeDir(preDir, dir):
                match = re.match(r"^([-_\.\+\w]+/)(.*)$", dir)
                if not match:
                    print("%sError: self.treeDir is badly defined.%s" %
                          (Back.RED + Fore.BLACK, Style.RESET_ALL))
                    return False
                if not os.path.exists(preDir + match.group(1)):
                    print("%sMaking directory %s%s%s" %
                          (Fore.MAGENTA, Style.BRIGHT, preDir + match.group(1),
                           Style.RESET_ALL))
                    os.mkdir(preDir + match.group(1))
                if match.group(2):
                    makeTreeDir(preDir + match.group(1), match.group(2))

            def mysqlText(text):
                text = re.sub(r"\.", r"\.", text)
                return text
            # Loop on parameters
            for p in self.params:
                if self.params[p] is None:
                    print("%sError: Some parameters of the calculation"
                          " are None, bad SetParams()%s" %
                          (Back.RED + Fore.BLACK, Style.RESET_ALL))
                    return False
                self.treeDir = re.sub(r"\$\(%s\)" % mysqlText(p),
                                      str(self.params[p]), self.treeDir)
                # Loop on previous datas
                for p in prevData:
                    if re.search(r"&\(%s\)" % mysqlText(p), self.treeDir):
                        if prevData[p] is None:
                            print("%sError: Some data from the previous"
                                  " calculationn are None%s" %
                                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
                            return False
                        # Replace in string
                        self.treeDir = re.sub(r"&\(%s\)" % mysqlText(p),
                                              str(prevData[p]), self.treeDir)
                if len(self.funcParams):
                    for fP in self.funcParams:
                        print("funcParams: fP = %s", fP)
                        self.treeDir = re.sub(r"!\(%s\)" % mysqlText(fP),
                                              self.funcParams[fP](),
                                              self.treeDir)
            makeTreeDir(self.conn.groundDir + self.baseDir, self.treeDir)

        if self.id != -1:
            finalDir = self.conn.groundDir + self.cells["%s.%sdir" %
                                                        (self.table,
                                                         self.table)]
        else:
            finalDir = self.conn.groundDir + self.baseDir + self.treeDir

        if clean:
            shutil.rmtree(finalDir)
            os.mkdir(finalDir)

        self.DatabaseAction(clean=clean)

        # Recursive method for directory, start with self.tree
        if not MakeDir(newTree, finalDir):
            print("%sError: Could not make the tree%s" %
                  (Back.RED + Fore.BLACK, Style.RESET_ALL))
            return False

        self.conn.Query("execute",
                        "UPDATE %s SET status = 'ready' WHERE %sid = %d" %
                        (self.table, self.table, self.id))
        print("%sSuccessfully made the tree,"
              " calculation is now ready to run%s" % (Fore.CYAN, Fore.RESET))
        self.DatabaseAction(status="ready")

    # Test that the tree is well built, might get replaced by LoadTree
    # function with automatic check up
    def TestTree(self):
        '''
        Verifies that a tree is well built, returns True or False
        '''
        return 0

    # Making the Qsub file for calculations requiering reserving a node
    def MakeQsub(self):
        '''
        Make the qsub, launch option
        Perhaps use MakeFile function
        '''
        pass


# Makes a file, might not be necessary
def MakeFile(filepath, filetext):
    '''
    Requiered:
        self.filepath   where to make the file
        self.filetext   what to print, each lines
    '''
    pass
