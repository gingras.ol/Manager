Manager Project
===============

Welcome to the Manager Project.


Installation
------------

To install, clone repository::

  >> git clone git@gitlab.com:gingras.ol/Manager.git
  >> cd Manager

Install requirements and optional requirements if needed (for development purposes)::

  >> pip install -r requirements.txt
  >> pip install -r requirements_optional.txt

Manager needs the `abisuite <https://gitlab.com/fgoudreault/abisuite>`__
project which is not on pypi thus can't be installed easily using pip.
Need to install it manually, in any directory execute the following::

  >> git clone git@gitlab.com:abiproject/abisuite.git
  >> cd abisuite
  >> pip install -r requirements.txt
  >> pip install -r requirements_optional.txt  # (optional)
  >> pip install -e .  # ('-e' flag optional)

Install the Manager package (come back to the Manager repository)::

  >> pip install .

For a development installation, use the '-e' flag::

  >> pip install -e .

In order to use the database part of the Manager module, you need a python connector for a MySQL
Database. Such a connector can be found `here <https://dev.mysql.com/downloads/connector/python/>`__.
Just download the source code, unzip it and install it with its setup script::

  >> python setup.py install

Unittesting
-----------

There is unittests written for the Manager Project. To execute them, just
run pytest at the top directory (where the setup.py file is located)::

  >> pytest

This module will find the tests and execute them. The needed packages
for development purposes are automatically installed when executing the
setup.py script with the 'develop' argument.
